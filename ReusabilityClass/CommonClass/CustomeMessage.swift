//
//  CustomeMessage.swift
//  Shopfeur Users
//
//  Created by deepkohli on 07/11/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import Foundation



let BASEURL = "https://webmobril.org/dev/broski_app/api/api"
let ImageBaseUrl = "https://webmobril.org/dev/broski_app/"

let APILOgin = "/signin"
let APISignUp = "/signup"
let APIForgotPassword = "/forgetpassword"
let APIResetPassword = "/resetpassword"
let APILOGOUT = "/api_logout"
let UPDATEPROFILE = "/updateprofile"
let VERIFYOTP = "/verifyotp_resetpassword"
let RESENDOTP = "/resendotp"
let CONTACTADMIN = "/contactus"
let APICHANGEPassword = "/resetpassword"



let AppName = "Broski"
let DeviceType = "2"

let EmptyOTPField = "Please enter OTP"
let EmptyEmail = "Please enter email address"
let EmptyPassword = "Please enter password"
let InvalidEmail = "Please enter valid email address"
let PasswordLength = "Password must be of 8 characters"

let EmptyFullName = "Please enter full name"
let EmptyConfirmPassword = "Please enter confirm password"
let NewPassword = "Please enter new password"
let EmptyAccessCode = "Please enter access code"
let PasswordMatch = "Password doesn't match"
let OLDPassword = "Please enter old password"
let MessageTitle = "Please enter message title"
let EmptyProfileImage = "Please select profile image"
let EmptyFirstName = "Please enter first name"
let EmptyLastName = "Please enter last name"

let appDelegate = UIApplication.shared.delegate as! AppDelegate

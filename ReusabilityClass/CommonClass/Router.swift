//
//  Router.swift


import UIKit

class Router: NSObject {
    private static let sharedOjbect = Router()
    class func sharedInst() -> Router
    {
        return sharedOjbect
    }
    
    func getViewControler(storyBoard: StoryboardType, indentifier: String) -> UIViewController
    {
        let storyB = UIStoryboard(name: storyBoard.rawValue, bundle: nil)
        return storyB.instantiateViewController(withIdentifier: indentifier)
    }
    
    func getLoginView() -> LoginVC {
        if let viewC = self.getViewControler(storyBoard: .Main, indentifier: LoginVC.className) as? LoginVC {
            return viewC
        } else {
            fatalError("Not able to initialize Branch TabBar View")
        }
    }
    func getSignUpView() -> SignUpVwC {
        if let viewC = self.getViewControler(storyBoard: .Main, indentifier: SignUpVwC.className) as? SignUpVwC {
            return viewC
        } else {
            fatalError("Not able to initialize Branch TabBar View")
        }
    }
    func getForgotPasswordView() -> ForgotPasswordVC {
        if let viewC = self.getViewControler(storyBoard: .Main, indentifier: ForgotPasswordVC.className) as? ForgotPasswordVC {
            return viewC
        } else {
            fatalError("Not able to initialize Branch TabBar View")
        }
    }
    func getresetPasswordView() -> ResetPasswordVC {
        if let viewC = self.getViewControler(storyBoard: .Main, indentifier: ResetPasswordVC.className) as? ResetPasswordVC {
            return viewC
        } else {
            fatalError("Not able to initialize Branch TabBar View")
        }
    }
    func getHomeView() -> HomeVC {
        if let viewC = self.getViewControler(storyBoard: .Main, indentifier: HomeVC.className) as? HomeVC {
            return viewC
        } else {
            fatalError("Not able to initialize Branch TabBar View")
        }
    }
    func getAccountSettingsView() -> AccountSettingsVC {
        if let viewC = self.getViewControler(storyBoard: .Main, indentifier: AccountSettingsVC.className) as? AccountSettingsVC {
            return viewC
        } else {
            fatalError("Not able to initialize Branch TabBar View")
        }
    }
    func getEditProfileView() -> EditProfileVC {
        if let viewC = self.getViewControler(storyBoard: .Main, indentifier: EditProfileVC.className) as? EditProfileVC {
            return viewC
        } else {
            fatalError("Not able to initialize Branch TabBar View")
        }
    }
    func getWebViewView() -> WebViewVC {
        if let viewC = self.getViewControler(storyBoard: .Main, indentifier: WebViewVC.className) as? WebViewVC {
            return viewC
        } else {
            fatalError("Not able to initialize Branch TabBar View")
        }
    }
   func getContactUsView() -> ContactUsVC {
       if let viewC = self.getViewControler(storyBoard: .Main, indentifier: ContactUsVC.className) as? ContactUsVC {
           return viewC
       } else {
           fatalError("Not able to initialize Branch TabBar View")
       }
   }
    func getChangePasswordView() -> ChangePasswordVC {
        if let viewC = self.getViewControler(storyBoard: .Main, indentifier: ChangePasswordVC.className) as? ChangePasswordVC {
            return viewC
        } else {
            fatalError("Not able to initialize Branch TabBar View")
        }
    }
    func getSpotifyPlayMusicView() -> SpotifyPlayMusicVC {
        if let viewC = self.getViewControler(storyBoard: .Main, indentifier: SpotifyPlayMusicVC.className) as? SpotifyPlayMusicVC {
            return viewC
        } else {
            fatalError("Not able to initialize Branch TabBar View")
        }
    }
    func getSpotifyLoginView() -> SpotifyLoginVC {
        if let viewC = self.getViewControler(storyBoard: .Main, indentifier: SpotifyLoginVC.className) as? SpotifyLoginVC {
            return viewC
        } else {
            fatalError("Not able to initialize Branch TabBar View")
        }
    }
    func getMusicTrackListView() -> MusicTrackListVC {
        if let viewC = self.getViewControler(storyBoard: .Main, indentifier: MusicTrackListVC.className) as? MusicTrackListVC {
            return viewC
        } else {
            fatalError("Not able to initialize Branch TabBar View")
        }
    }
    func getMediaSearchTableView() -> MediaSearchTableViewController {
        if let viewC = self.getViewControler(storyBoard: .Main, indentifier: MediaSearchTableViewController.className) as? MediaSearchTableViewController {
            return viewC
        } else {
            fatalError("Not able to initialize Branch TabBar View")
        }
    }
    
    func getTrackListView() -> TrackListVC {
        if let viewC = self.getViewControler(storyBoard: .Main, indentifier: TrackListVC.className) as? TrackListVC {
            return viewC
        } else {
            fatalError("Not able to initialize Branch TabBar View")
        }
    }
    func getTableControllerView() -> TableViewController {
        if let viewC = self.getViewControler(storyBoard: .Main, indentifier: TableViewController.className) as? TableViewController {
            return viewC
        } else {
            fatalError("Not able to initialize Branch TabBar View")
        }
    }
    func getiTunePlayerView() -> ITunesPlayerVC {
        if let viewC = self.getViewControler(storyBoard: .Main, indentifier: ITunesPlayerVC.className) as? ITunesPlayerVC {
            return viewC
        } else {
            fatalError("Not able to initialize Branch TabBar View")
        }
    }
    func getAudioView() -> AudioVC {
        if let viewC = self.getViewControler(storyBoard: .Main, indentifier: AudioVC.className) as? AudioVC {
            return viewC
        } else {
            fatalError("Not able to initialize Branch TabBar View")
        }
    }
    func getPlayView() -> PlayVC {
        if let viewC = self.getViewControler(storyBoard: .Main, indentifier: PlayVC.className) as? PlayVC {
            return viewC
        } else {
            fatalError("Not able to initialize Branch TabBar View")
        }
    }
    
    
}

enum StoryboardType: String {
    
    case LaunchScreen
    case Main
    
    var storyboardName: String {
        return rawValue
    }
}

//
//  FXUtility.swift
//  Fisheries
//
//  Created by Anuj on 26/12/18.
//  Copyright © 2018 Coastalwatch Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class FXUtility: NSObject {
    class func rootViewController() -> UIViewController {
        return (UIApplication.shared.keyWindow?.rootViewController)!
    }
    // MARK: - Get topmost view controller
    class func topMostViewController(rootViewController: UIViewController) -> UIViewController? {
        if let navigationController = rootViewController as? UINavigationController {
            return topMostViewController(rootViewController: navigationController.visibleViewController!)
        }
        if let tabBarController = rootViewController as? UITabBarController {
            if let selectedTabBarController = tabBarController.selectedViewController {
                return topMostViewController(rootViewController: selectedTabBarController)
            }
        }
        if let presentedViewController = rootViewController.presentedViewController {
            return topMostViewController(rootViewController: presentedViewController)
        }
        return rootViewController
    }
    class func visibleController() -> UIViewController {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
           
            return topController
        }
        return UIViewController()
    }
    class func sumDigits(num: Float) -> Float {
        return String(num).compactMap { Float(String($0)) }.reduce(0, +)
    }
}

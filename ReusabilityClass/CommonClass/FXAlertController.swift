//
//  FXAlertController.swift
//  Fisheries
//
//  Created by Anuj on 26/12/18.
//  Copyright © 2018 Coastalwatch Pty Ltd. All rights reserved.
//

import Foundation

import UIKit
    @objc open class FXAlertController: NSObject {
        
        
        class func isValidEmail(testStr:String)-> Bool
        {
            
            let emailRegEx = "[A-Z0-9a-z._-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailTest.evaluate(with: testStr)
        }
        
        /*------ Validate Alfanumaric Password ------*/
        class func isValidPassword(testStr:String)-> Bool
        {
            let passwordRegEx = "^([a-zA-Z0-9@*#]{8,20})$"
            let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
            return passwordTest.evaluate(with: testStr)
        }
        //==========================================================================================================
        // MARK: - Singleton
        //==========================================================================================================
        
        class var instance : FXAlertController {
            struct Static {
                static let inst : FXAlertController = FXAlertController ()
            }
            return Static.inst
        }
    
        //==========================================================================================================
        // MARK: - Class Functions
        //==========================================================================================================
        
    
        @discardableResult
        open class func alert(_ title: String) -> UIAlertController {
            return alert(title, message: "")
        }
        
        @discardableResult
        open class func alert(_ title: String, message: String) -> UIAlertController {
            return alert(title, message: message, acceptMessage: "OK", acceptBlock: {
                // Do nothing
            })
        }
        @discardableResult
        open class func alert(_ title: String, message: String, buttons:[String], tapBlock:((UIAlertController, UIAlertAction, Int) -> Void)?) -> UIAlertController{
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert, buttons: buttons, tapBlock: tapBlock)
            FXUtility.visibleController().present(alert, animated: true, completion: nil)
            return alert
        }
        @discardableResult
        open class func alert(_ title: String, message: String, textFieldPlaceholders: [String], buttons:[String], tapBlock:((UIAlertController, UIAlertAction, Int) -> Void)?) -> UIAlertController {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert, textFieldPlaceholders: textFieldPlaceholders, buttons: buttons, tapBlock: tapBlock)
            FXUtility.visibleController().present(alert, animated: true, completion: nil)
            return alert
        }
        @discardableResult
        open class func alert(_ title: String, message: String, acceptMessage: String, acceptBlock: @escaping () -> ()) -> UIAlertController {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let acceptButton = UIAlertAction(title: acceptMessage, style: .default, handler: { (action: UIAlertAction) in
                acceptBlock()
            })
            alert.addAction(acceptButton)
            FXUtility.visibleController().present(alert, animated: true, completion: nil)
            return alert
        }
        @discardableResult
        open class func actionSheet(_ title: String? = nil, message: String? = nil, sourceView: UIView, buttons:[String], tapBlock:((UIAlertController, UIAlertAction, Int) -> Void)?) -> UIAlertController{
            let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet, buttons: buttons, tapBlock: tapBlock)
            alert.popoverPresentationController?.sourceView = sourceView
            alert.popoverPresentationController?.sourceRect = sourceView.bounds
            FXUtility.visibleController().present(alert, animated: true, completion: nil)
            return alert
        }
        @discardableResult
        open class func alertSheet(_ title: String? = nil, message: String? = nil, sourceView: UIView, buttons:[String], tapBlock:((UIAlertController, UIAlertAction, Int) -> Void)?) -> UIAlertController{
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert, buttons: buttons, tapBlock: tapBlock)
            alert.popoverPresentationController?.sourceView = sourceView
            alert.popoverPresentationController?.sourceRect = sourceView.bounds
            FXUtility.visibleController().present(alert, animated: true, completion: nil)
            return alert
        }
    }
    private extension UIAlertController {
        convenience init(title: String?, message: String?, preferredStyle: UIAlertController.Style, textFieldPlaceholders: [String]? = nil, buttons:[String], tapBlock:((UIAlertController, UIAlertAction,Int) -> Void)?) {
            self.init(title: title, message: message, preferredStyle:preferredStyle)
            var buttonIndex = 0
            for buttonTitle in buttons {
                var preferredStyle: UIAlertAction.Style!
                switch buttonTitle.lowercased() {
                case "cancel":
                    preferredStyle = .cancel
                default:
                    preferredStyle = .default
                }
                let action = UIAlertAction(title: buttonTitle, preferredStyle: preferredStyle, alert: self, buttonIndex: buttonIndex, tapBlock: tapBlock)
                buttonIndex += 1
                self.addAction(action)
            }
            guard let placeholders = textFieldPlaceholders else { return }
            for placeholder in placeholders {
                self.addTextField(configurationHandler: { (textField) in
                    textField.placeholder = placeholder
                    textField.isSecureTextEntry = placeholder.lowercased().contains("password")
                })
            }
        }
    }
    private extension UIAlertAction {
        convenience init(title: String?, preferredStyle: UIAlertAction.Style, alert: UIAlertController, buttonIndex:Int, tapBlock:((UIAlertController, UIAlertAction, Int) -> Void)?) {
            self.init(title: title, style: preferredStyle) {
                (action:UIAlertAction) in
                if let block = tapBlock {
                    block(alert, action, buttonIndex)
                }
            }
        }
    }

    


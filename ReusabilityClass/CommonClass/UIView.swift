//
//  UIView.swift
//  EndCashBusiness
//
//  Created by Anuj on 05/02/19.
//  Copyright © 2019 Flexsin. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func dropBorder() {
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor(red: 35/255, green: 31/255, blue: 32/255, alpha: 1.0).cgColor
        self.layer.cornerRadius = 7.0
        self.clipsToBounds = true
    }
    
    func dropShadow() {
//        self.layer.masksToBounds = false
//        self.layer.shadowColor = UIColor.gray.cgColor
//        self.layer.shadowOpacity = 0.5
//        self.layer.shadowOffset = CGSize(width: -1, height: 1)
//        self.layer.shadowRadius = 1
//        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
//        self.layer.shouldRasterize = true
//        self.layer.rasterizationScale = UIScreen.main.scale
        
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 1.0
        self.layer.shadowOpacity = 0.5
        self.layer.cornerRadius = 7.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        //self.layer.borderColor = UIColor(red: 245/255, green: 246/255, blue: 250/255, alpha: 1.0).cgColor
        
    }
    func dropCellShadow()  {
//        self.layer.shadowColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1.0).cgColor //UIColor.gray.cgColor
//        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
//        self.layer.masksToBounds = false
//       self.layer.shadowRadius = 1.0
//        self.layer.shadowOpacity = 0.5
//        self.layer.cornerRadius = 5.0
        
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 1.0
        self.layer.shadowOpacity = 1.0
        self.layer.cornerRadius = 5.0
    }
    
    class func initFromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: self), owner: nil, options: nil)?[0] as! T
    }
    
    func addDashedLineBorder() {
        let color = UIColor.green.cgColor
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = (self.frame.size)
        let shapeRect = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 2
        shapeLayer.cornerRadius = 10
       // shapeLayer.lineJoin = kCALineJoinRound
        shapeLayer.lineDashPattern = [12,12]
        shapeLayer.path = UIBezierPath(rect: shapeRect).cgPath
        self.layer.addSublayer(shapeLayer)
    }
    
    
    
}

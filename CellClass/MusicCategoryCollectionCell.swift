//
//  MusicCategoryCollectionCell.swift
//  Broski
//
//  Created by deepkohli on 26/12/19.
//  Copyright © 2019 Webmobril.com. All rights reserved.
//

import UIKit

class MusicCategoryCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryIcon: UIImageView!
    @IBOutlet weak var checkIcon: UIImageView!
    @IBOutlet weak var categoryName: UILabel!
    
}

//
//  PlayListCell.swift
//  Broski
//
//  Created by deepkohli on 13/01/20.
//  Copyright © 2020 Webmobril.com. All rights reserved.
//

import UIKit

class PlayListCell: UITableViewCell {
    
    @IBOutlet weak var mainImageView : UIImageView!
    @IBOutlet weak var musicNameLabel : UILabel!
    @IBOutlet weak var musictypeLabel : UILabel!
   

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  AudioManager.swift
//  Broski
//
//  Created by deepkohli on 23/01/20.
//  Copyright © 2020 Webmobril.com. All rights reserved.
//

import Foundation
import AVFoundation

class AudioManager {
  static let sharedInstance = AudioManager()

  var musicPlayer: AVAudioPlayer?
  var botaoApertado: AVAudioPlayer?
  var player = AVAudioPlayer()
  var isCheckPausePlayBtn = false

  private init() {
  }

    func startMusic(url:URL) {
    do {
        isCheckPausePlayBtn = true
        player = try AVAudioPlayer(contentsOf: url)
        player.prepareToPlay()
       // player.set
        player.play()

    }
    catch{
        print(error)
    }
  }
    func stopMusic() {
        if player.isPlaying {
            isCheckPausePlayBtn = false
            player.pause()
        }
        else{
            isCheckPausePlayBtn = true
            player.play()
        }
    }
    func closeMusic() {
        player.pause()
    }

}









//
//  MediaSearchTableViewController.swift
//  Broski
//
//  Created by deepkohli on 12/02/20.
//  Copyright © 2020 Webmobril.com. All rights reserved.
//

import UIKit
import StoreKit

class MediaSearchTableViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
       var searchController = UISearchController(searchResultsController: nil)
       var authorizationManager: AuthorizationManager!
       let appleMusicManager = AppleMusicManager()
       var mediaLibraryManager: MediaLibraryManager!
       var setterQueue = DispatchQueue(label: "MediaSearchTableViewController")
       var mediaItems = [[MediaItem]]() {
           didSet {
               DispatchQueue.main.async {
                   self.tableView.reloadData()
               }
           }
       }
    
    lazy var tempauthorizationManager: AuthorizationManager = {
        return AuthorizationManager(appleMusicManager: self.appleMusicManager)
    }()
    
    /// The instance of `MediaLibraryManager` which manages the `MPPMediaPlaylist` this application creates.
    lazy var tempmediaLibraryManager: MediaLibraryManager = {
        return MediaLibraryManager(authorizationManager: self.authorizationManager)
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        
        // Configure the `UISearchController`.
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        definesPresentationContext = true
        searchController.searchBar.delegate = self
        tableView.tableHeaderView = searchController.searchBar
        
       authorizationManager = tempauthorizationManager
       mediaLibraryManager = tempmediaLibraryManager
        
        /*
         Add the notification observers needed to respond to events from the `AuthorizationManager`, `MPMediaLibrary` and `UIApplication`.
         This is so that if the user enables/disables capabilities in the Settings app the application will reflect those changes accurately.
         */
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(handleAuthorizationManagerAuthorizationDidUpdateNotification),
                                       name: AuthorizationManager.authorizationDidUpdateNotification,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(handleAuthorizationManagerAuthorizationDidUpdateNotification),
                                       name: UIApplication.willEnterForegroundNotification,
                                       object: nil)
        
    }
    deinit {
        // Remove all notification observers.
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self, name: AuthorizationManager.authorizationDidUpdateNotification, object: nil)
        notificationCenter.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if appleMusicManager.fetchDeveloperToken() == nil {
            
            searchController.searchBar.isUserInteractionEnabled = false
            
            let alertController = UIAlertController(title: "Error",
                                                    message: "No developer token was specified. See the README for more information.",
                                                    preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.cancel, handler: nil))
            present(alertController, animated: true, completion: nil)
        } else {
            searchController.searchBar.isUserInteractionEnabled = true
        }
    }
     @IBAction func action_Back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    // MARK: - Table view data source
     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return mediaItems.count
    }
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return mediaItems[section].count
    }
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MediaItemTableViewCell.identifier,
                                                       for: indexPath) as? MediaItemTableViewCell else {
                                                        return UITableViewCell()
        }
        
        let mediaItem = mediaItems[indexPath.section][indexPath.row]
        cell.mediaItem = mediaItem
        cell.delegate = self
        let cloudServiceCapabilities = authorizationManager.cloudServiceCapabilities
        
        /*
        It is important to actually check if your application has the appropriate `SKCloudServiceCapability` options before enabling functionality
         related to playing back content from the Apple Music Catalog or adding items to the user's Cloud Music Library.
         */
        
        if cloudServiceCapabilities.contains(.addToCloudMusicLibrary) {
            cell.addToPlaylistButton.isEnabled = true
        } else {
            cell.addToPlaylistButton.isEnabled = false
        }
        if cloudServiceCapabilities.contains(.musicCatalogPlayback) {
            cell.playItemButton.isEnabled = true
        } else {
            cell.playItemButton.isEnabled = false
        }
        return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return NSLocalizedString("Songs", comment: "Songs")
        } else {
            return NSLocalizedString("Albums", comment: "Albums")
        }
    }
    @objc func handleAuthorizationManagerAuthorizationDidUpdateNotification() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MediaSearchTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchString = searchController.searchBar.text else {
            return
        }
        
        if searchString == "" {
            self.setterQueue.sync {
                self.mediaItems = []
            }
        } else {
            appleMusicManager.performAppleMusicCatalogSearch(with: searchString,
                                                             countryCode: authorizationManager.cloudServiceStorefrontCountryCode,
                                                             completion: { [weak self] (searchResults, error) in
                guard error == nil else {
                    
                    // Your application should handle these errors appropriately depending on the kind of error.
                    self?.setterQueue.sync {
                        self?.mediaItems = []
                    }
                    
                    let alertController: UIAlertController
                    
                    guard let error = error as NSError?, let underlyingError = error.userInfo[NSUnderlyingErrorKey] as? Error else {
                        
                        alertController = UIAlertController(title: "Error",
                                                            message: "Encountered unexpected error.",
                                                            preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                        
                        DispatchQueue.main.async {
                            self?.present(alertController, animated: true, completion: nil)
                        }
                        
                        return
                    }
                    
                    alertController = UIAlertController(title: "Error",
                                                        message: underlyingError.localizedDescription,
                                                        preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                    
                    DispatchQueue.main.async {
                        self?.present(alertController, animated: true, completion: nil)
                    }
                    
                    return
                }
                                                                
                self?.setterQueue.sync {
                    self?.mediaItems = searchResults
                }
                                                                
            })
        }
    }
}
extension MediaSearchTableViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        setterQueue.sync {
            self.mediaItems = []
        }
    }
}

extension MediaSearchTableViewController: MediaSearchTableViewCellDelegate {
    func mediaSearchTableViewCell(_ mediaSearchTableViewCell: MediaItemTableViewCell, addToPlaylist mediaItem: MediaItem) {
        mediaLibraryManager.addItem(with: mediaItem.identifier)
    }
}

//
//  ViewController.swift
//  Broski
//
//  Created by deepkohli on 23/12/19.
//  Copyright © 2019 Webmobril.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class ViewController: UIViewController,NVActivityIndicatorViewable,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
/********  GET METHOD  ***********/
        
         if Reachability.isConnectedToNetwork() {
        let size = CGSize(width: 30, height: 30)
        self.startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
        // let user_id = UserDefaults.standard.value(forKey: "user_id") as? String ?? ""
    let urlString = String(format: "http://webmobril.org/dev/xtensions4us/api/api_reminder?user_id=%@&product_id=%@","","")
    print(urlString)
    AF.request(urlString, method: .get, encoding: JSONEncoding.default, headers: nil).responseJSON {
                        response in
                        switch response.result {
                         case .success(let value):
                            print(response)
                            
                            let json = value as? [String: Any]
                            
                            let errorres = json!["error"] as! Bool
                            
                            if errorres {
                                let message = json!["message"] as! String
                            }
                            
                            DispatchQueue.main.async {
                              self.stopAnimating(nil)
                            }
                            break
                            
                        case .failure(let error):
                            print(error)
                            DispatchQueue.main.async {
                           self.stopAnimating(nil)
                            }
                        }//
                    }
                }
        
    }
    func makePayment() {
            self.view.endEditing(true)
         if Reachability.isConnectedToNetwork() {
                let login: String = "api_make_payment"
                let apiURL: String = ""
     let user_id = UserDefaults.standard.value(forKey: "user_id") as? String ?? ""
        let newTodo: [String: Any] = [:]
                let size = CGSize(width: 30, height: 30)
                startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
                AF.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in
                    
                    switch response.result {
                        case .success(let value):
                            print(response)
                    let json = value as? [String: Any]
                    let errorres = json!["error"] as! Bool
                        if errorres {
                    let message = json!["message"] as! String
                        }
                    DispatchQueue.main.async {
                        self.stopAnimating(nil)
                        }
                break
            case .failure(let error):
                    print(error)
            DispatchQueue.main.async {
            self.stopAnimating(nil)
                    }
            }
        }
        }
            
        }

}
/*
@IBAction func action_camerabutt(_ sender: Any) {
       self.view.endEditing(true)
       let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
       alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
           self.openCamera()
       }))
       
       alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
           self.openGallary()
       }))
       
       alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
       self.present(alert, animated: true, completion: nil)
   }
   
   func openCamera() {
       if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
           let imagePicker = UIImagePickerController()
           imagePicker.delegate = self
           imagePicker.sourceType = UIImagePickerController.SourceType.camera
           imagePicker.allowsEditing = true
           self.present(imagePicker, animated: true, completion: nil)
       }
       else {
           let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
           alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
           self.present(alert, animated: true, completion: nil)
       }
   }
   
   func openGallary() {
       let imagePicker = UIImagePickerController()
       imagePicker.delegate = self
       imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
       imagePicker.allowsEditing = true
       self.present(imagePicker, animated: true, completion: nil)
   }
   
   func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
       
   }
   
   public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
       let image = info[.originalImage] as! UIImage
       self.dismiss(animated: true, completion: { () -> Void in
           
           var imageCropVC : RSKImageCropViewController!
           imageCropVC = RSKImageCropViewController(image: image, cropMode: RSKImageCropMode.circle)
           imageCropVC.delegate = self
           self.navigationController?.pushViewController(imageCropVC, animated: true)
           
       })
       
   }
   
   func nullToNil(value : AnyObject?) -> AnyObject? {
       if value is NSNull {
           return nil
       } else {
           return value
       }
   }
*/
/*
extension ViewController: RSKImageCropViewControllerDelegate {
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
       
        _ = self.navigationController?.popViewController(animated: true)
        let apiURL: String = "http://webmobril.org/dev/xtensions4us/api/api_update_profile_image"
        print("\(apiURL)")
        let user_id = UserDefaults.standard.object(forKey: "user_id") as! String
        let imgData = Data()
        let newTodo: [String: Any] = ["user_id":user_id]
        print("parameters are \(newTodo)")
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: "user_image",fileName: "profile.jpg", mimeType: "image/jpg")
            
            for (key, value) in newTodo {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:apiURL) {
            (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    let json = response.result.value as? [String: Any]
                    let errorres = json!["error"] as! Bool
                    
                    if errorres {
                        let message = json!["message"] as! String
                        let alert = UIAlertController(title: "X4us", message: message, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler:  { action in
                        }))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        let message = json!["message"] as! String
                        let alert = UIAlertController(title: "X4us", message: message, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler:  { action in
                           
                            let cell0:LeftMenuTVC = self.tabkeview_userprofile .cellForRow(at: IndexPath.init(row: 0, section: 0)) as! LeftMenuTVC
                            cell0.userprofile_camerabutt.isHidden = true
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    self.stopAnimating(nil)
                    
                }
            case .failure(let encodingError):
                print(encodingError)
                self.stopAnimating(nil)
                
            }
        }
    }
    
    
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    
}

*/

//
//  AccountSettingsModel.swift
//  Broski
//
//  Created by deepkohli on 13/03/20.
//  Copyright © 2020 Webmobril.com. All rights reserved.
//

import UIKit

class AccountSettingsModel: NSObject {
    
    var firstName: String!
    var lastName: String!
    var fullName: String!
    var email: String!
    var userImage: String!
    
    static func getUserDetailsData(response:[String:Any]) -> Array<AccountSettingsModel>{
        var modelArray = Array<AccountSettingsModel>()
        let modelDict = AccountSettingsModel()
        if response["fname"] as? String != nil{
            modelDict.firstName = (response["fname"] as! String)
        }
        else{
            modelDict.firstName = ""
        }
        if response["lname"] as? String != nil{
             modelDict.lastName = (response["lname"] as! String)
        }
        else{
            modelDict.lastName = ""
        }
        if response["name"] as? String != nil{
             modelDict.fullName = (response["name"] as! String)
        }
        if response["email"] as? String != nil{
             modelDict.email = (response["email"] as! String)
        }
        if response["user_profile"] as? String != nil{
             modelDict.userImage = (response["user_profile"] as! String)
        }
        else{
            modelDict.userImage = ""
        }
        modelArray.append(modelDict)
        return modelArray
    }

}

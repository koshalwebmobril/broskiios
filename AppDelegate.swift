//
//  AppDelegate.swift
//  Broski
//
//  Created by deepkohli on 23/12/19.
//  Copyright © 2019 Webmobril.com. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Fabric
import Crashlytics
import AVFoundation

//import SpotifyLogin
//import SpotifyiOS

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{

    var window: UIWindow?
   // var reachability: Reachability!
    var rootViewController = UIViewController()
    var auth = SPTAuth()
    
    /// The instance of `AuthorizationManager` which is responsible for managing authorization for the application.
    lazy var authorizationManager: AuthorizationManager = {
        return AuthorizationManager(appleMusicManager: self.appleMusicManager)
    }()
    
    /// The instance of `MediaLibraryManager` which manages the `MPPMediaPlaylist` this application creates.
    lazy var mediaLibraryManager: MediaLibraryManager = {
        return MediaLibraryManager(authorizationManager: self.authorizationManager)
    }()

    /// The instance of `AppleMusicManager` which handles making web service calls to Apple Music Web Services.
    var appleMusicManager = AppleMusicManager()
    
   
    
    class var sharedInstance: AppDelegate {
        get {
            return UIApplication.shared.delegate as! AppDelegate
        }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
       // if #available(iOS 13.0, *) {
       // window?.overrideUserInterfaceStyle = .light
       // }
        
        auth.redirectURL = URL(string: "Broski://returnAfterLogin") // insert your redirect URL here
        auth.sessionUserDefaultsKey = "current session"
        
        IQKeyboardManager.shared.enable = true
        let isLogin = UserDefaults.standard.bool(forKey: "IsLogin")
        if isLogin{
            homeView()
        }else{
            loginView()
        }
        
        return true
    }
    
    func loginView() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
         let rootViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let navigationController = UINavigationController(rootViewController: rootViewController)
        navigationController.isNavigationBarHidden = true
        //let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }
    func homeView() {
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //rootViewController = mainStoryboard.instantiateViewController(withIdentifier: "SpotifyLoginVC") as! SpotifyLoginVC
        //rootViewController = mainStoryboard.instantiateViewController(withIdentifier: "AccountSettingsVC") as! AccountSettingsVC
          rootViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let navigationController = UINavigationController(rootViewController: rootViewController)
        navigationController.isNavigationBarHidden = true
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        appDelegate.window!.rootViewController = navigationController
        appDelegate.window!.makeKeyAndVisible()
    }
    class func getAppdelegate()->AppDelegate
    {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
//    internal func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
//        do {
//            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: [.mixWithOthers, .allowAirPlay])
//            print("Playback OK")
//            try AVAudioSession.sharedInstance().setActive(true)
//            print("Session is Active")
//        } catch {
//            print(error)
//        }
//        return true
//    }
    //MARK: Audio Session Mixing
//    func allowBackgroundAudio()
//    {
//        do {
//            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, withOptions: .MixWithOthers)
//        } catch {
//            NSLog("AVAudioSession SetCategory - Playback:MixWithOthers failed")
//        }
//    }
//
//    func preventBackgroundAudio()
//    {
//        do {
//            //Ask for Solo Ambient to prevent any background audio playing, then change to normal Playback so we can play while locked
//            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategorySoloAmbient)
//            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
//        } catch {
//            NSLog("AVAudioSession SetCategory - SoloAmbient failed")
//        }
//    }
//    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        // called when user signs into spotify. Session data saved into user defaults, then notification posted to call updateAfterFirstLogin in ViewController.swift. Modeled off recommneded auth flow suggested by Spotify documentation
        if auth.canHandle(auth.redirectURL) {
            auth.handleAuthCallback(withTriggeredAuthURL: url, callback: { (error, session) in
                if error != nil {
                    print("error!")
                }
                let userDefaults = UserDefaults.standard
                let sessionData = NSKeyedArchiver.archivedData(withRootObject: session)
                print(sessionData)
                userDefaults.set(sessionData, forKey: "SpotifySession")
                userDefaults.synchronize()
                NotificationCenter.default.post(name: Notification.Name(rawValue: "loginSuccessfull"), object: nil)
            })
            return true
        }
        
        return false
    
    }
    
    
    
    
//    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
//        let parameters = appRemote.authorizationParameters(from: url);
//
//        if let access_token = parameters?[SPTAppRemoteAccessTokenKey] {
//            appRemote.connectionParameters.accessToken = access_token
//            self.accessToken = access_token
//        } else if let error_description = parameters?[SPTAppRemoteErrorDescriptionKey] {
//            playerViewController.showError(error_description);
//        }
//
//        return true
//    }
    
    func applicationWillResignActive(_ application: UIApplication) {
       
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
       
    }
    /*
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "SpotifyPlayMusicVC") as! SpotifyPlayMusicVC
        homeViewController.sessionManager.application(app, open: url, options: options)
        return true
    }
    func applicationWillResignActive(_ application: UIApplication) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "SpotifyPlayMusicVC") as! SpotifyPlayMusicVC
        if (homeViewController.appRemote.isConnected) {
            homeViewController.appRemote.disconnect()
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "SpotifyPlayMusicVC") as! SpotifyPlayMusicVC
        if let _ = homeViewController.appRemote.connectionParameters.accessToken {
            homeViewController.appRemote.connect()
        }
    }
 */
    
    
//    internal func application(_ app: UIApplication,
//                     open url: URL,
//                     options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
//        let handled = SpotifyLogin.shared.applicationOpenURL(url) { _ in }
//        return handled
//    }
    

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Broski")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}


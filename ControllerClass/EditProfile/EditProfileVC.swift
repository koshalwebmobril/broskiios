//
//  EditProfileVC.swift
//  Broski
//
//  Created by deepkohli on 31/12/19.
//  Copyright © 2019 Webmobril.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import YPImagePicker
import Photos
import SDWebImage

protocol updateProfileRecordDelegate {
    func updateProfileRecord(dict:[String:Any])
}

class EditProfileVC: UIViewController,NVActivityIndicatorViewable {
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var btn_update: UIButton!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var txt_FirstName: UITextField!
    @IBOutlet weak var txt_LastName: UITextField!
    
    var profileImageView = UIImageView()
    var delegate: updateProfileRecordDelegate?
    
    var modelArray: Array<AccountSettingsModel> = []

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInitialValue()
        setUPUserProfileData()
    }
    func setUpInitialValue(){
        btn_update.layer.cornerRadius = btn_update.frame.size.height / 2
        btn_update.clipsToBounds = true
        userImageView.layer.cornerRadius = userImageView.frame.size.height / 2
        userImageView.clipsToBounds = true
        userImageView.layer.borderWidth = 4.0
        userImageView.layer.borderColor = UIColor.white.cgColor
        emailView.dropBorder()
        passwordView.dropBorder()
    }
    func setUPUserProfileData(){
         if !modelArray[0].firstName.isEmpty{
            txt_FirstName.text = modelArray[0].firstName
         }
        if !modelArray[0].lastName.isEmpty{
           txt_LastName.text = modelArray[0].lastName
        }
        if modelArray[0].userImage.isEmpty{
           self.userImageView.image = UIImage(named: "user")
        }
        else{
            userImageView.sd_imageIndicator = SDWebImageActivityIndicator.white
            let image = modelArray[0].userImage
            userImageView.sd_setImage(with: URL(string:image!)) { (image, error, cache, urls) in
                            if (error != nil) {
                                self.userImageView.image = UIImage(named: "user")
                            } else {
                                self.userImageView.image = image
                                self.userImageView.contentMode = .scaleToFill
                                self.profileImageView.image = image
                }
            }
        }
        
    }

   func uploadImage(){
         if Reachability.isConnectedToNetwork() {
            let headers: HTTPHeaders = ["Content-Type": "application/json",
            "accept": "application/json"]
        let imageData = profileImageView.image?.jpegData(compressionQuality: 0.75)
        let user_id = UserDefaults.standard.value(forKey: "IdVal") as? String ?? ""
        let apiURL: String = "\(BASEURL)\(UPDATEPROFILE)"
        print("\(apiURL)")
        let newTodo: [String: Any] = ["userid":user_id,"fname":txt_FirstName.text!,"lname":txt_LastName.text!]
        print("parameters are \(newTodo)")
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
            
        AF.upload(multipartFormData: { multiPart in
            for (key, value) in newTodo {
                if let temp = value as? String {
                    multiPart.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    multiPart.append("\(temp)".data(using: .utf8)!, withName: key)
                }
                if let temp = value as? NSArray {
                    temp.forEach({ element in
                        let keyObj = key + "[]"
                        if let string = element as? String {
                            multiPart.append(string.data(using: .utf8)!, withName: keyObj)
                        } else
                            if let num = element as? Int {
                                let value = "\(num)"
                                multiPart.append(value.data(using: .utf8)!, withName: keyObj)
                        }
                    })
                }
            }
            multiPart.append(imageData!, withName: "user_profile",fileName: "profile.jpg", mimeType: "image/jpg")
        }, to: apiURL, method: .post , headers: headers)
            
            .uploadProgress(queue: .main, closure: { progress in
            print("Upload Progress: \(progress.fractionCompleted)")
            })
            .responseJSON(completionHandler: { data in
                switch data.result{
                 case .success(let value):
                   
                    let json = value as? [String: Any]
                    let errorres = json!["error"] as! Bool
                    if !errorres {
                    if json!["result"] as? [String: Any] != nil{
                        
                        let alertController = UIAlertController(title: AppName, message: "Profile Updated Successfully", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                            UIAlertAction in
                            self.delegate?.updateProfileRecord(dict: (json!["result"] as! [String: Any]))
                            self.navigationController?.popViewController(animated: true)
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                        }
                    }
                    DispatchQueue.main.async {
                      self.stopAnimating(nil)
                    }
                    break
                case .failure(let error):
                    print(error)
                    DispatchQueue.main.async {
                   self.stopAnimating(nil)
                    }
                }
                DispatchQueue.main.async {
                self.stopAnimating(nil)
                }
            })
    }
    }
    
     @IBAction func backBttnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
 
    @IBAction func btn_selectImageAction(_ sender: Any) {
           self.view.endEditing(true)
                var config = YPImagePickerConfiguration()
                config.library.onlySquare = true
                config.onlySquareImagesFromCamera = true
                config.targetImageSize = .cappedTo(size: 1024)
                config.library.mediaType = .photo
                config.usesFrontCamera = true
                config.showsPhotoFilters = true
                config.shouldSaveNewPicturesToAlbum = true
                config.startOnScreen = .library
                config.screens = [.library, .photo,]
                config.showsCrop = .none
                config.wordings.libraryTitle = "Photos"
                config.hidesStatusBar = true
                config.hidesBottomBar = false
                config.library.maxNumberOfItems = 1
           
                let picker = YPImagePicker(configuration: config)
                /* Single Photo implementation. */
                picker.didFinishPicking { [unowned picker] items, _ in
                    if items.singlePhoto?.image != nil {
                self.userImageView.image = items.singlePhoto?.image
                self.profileImageView.image = items.singlePhoto?.image
                    }
                picker.dismiss(animated: true, completion: nil)
                }
                present(picker, animated: true, completion: nil)
       }
    @IBAction func btn_updateProfileAction(_ sender: Any) {
        if dataValidation() {
        uploadImage()
        }
    }
    @IBAction func action_headPhone(_ sender: Any) {
           
       }
    @IBAction func action_power(_ sender: Any) {
           
       }
    @IBAction func action_setting(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func dataValidation() -> Bool {
    let whiteSpaces = CharacterSet.whitespacesAndNewlines
        if txt_FirstName.text!.isEmpty || txt_FirstName.text!.trimmingCharacters(in: whiteSpaces).isEmpty {
            FXAlertController.alert(AppName, message: EmptyFirstName)
            txt_FirstName.becomeFirstResponder()
            return false
        }
        if txt_LastName.text!.isEmpty || txt_LastName.text!.trimmingCharacters(in: whiteSpaces).isEmpty{
             FXAlertController.alert(AppName, message: EmptyLastName)
             txt_LastName.becomeFirstResponder()
            return false
        }
        if self.profileImageView.image == nil {
            FXAlertController.alert(AppName, message: EmptyProfileImage)
             return false
        }
        return true
    }

}

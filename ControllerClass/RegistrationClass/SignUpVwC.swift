//
//  SignUpVwC.swift
//  Broski
//
//  Created by deepkohli on 23/12/19.
//  Copyright © 2019 Webmobril.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class SignUpVwC: UIViewController,NVActivityIndicatorViewable {
    
     @IBOutlet weak var mainView: UIView!
     @IBOutlet weak var btn_signUp: UIButton!
     @IBOutlet weak var emailView: UIView!
     @IBOutlet weak var passwordView: UIView!
     @IBOutlet weak var confirmPasswordView: UIView!
     @IBOutlet weak var fullNameView: UIView!
     @IBOutlet weak var accessCodeView: UIView!
     @IBOutlet weak var txt_email: UITextField!
     @IBOutlet weak var txt_password: UITextField!
     @IBOutlet weak var txt_confirmPassword: UITextField!
     @IBOutlet weak var txt_fullName: UITextField!
     @IBOutlet weak var txt_accessCode: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInitialValue()
    }
    func setUpInitialValue(){
        btn_signUp.layer.cornerRadius = btn_signUp.frame.size.height / 2
        btn_signUp.clipsToBounds = true
        emailView.dropBorder()
        passwordView.dropBorder()
        confirmPasswordView.dropBorder()
        fullNameView.dropBorder()
        accessCodeView.dropBorder()
        mainView.dropShadow()
    }
    //MARK:-- Validate input fields.
    private func areValidInputs() -> Bool {
        let whiteSpaces = CharacterSet.whitespacesAndNewlines
        if txt_fullName.text!.isEmpty || txt_fullName.text!.trimmingCharacters(in: whiteSpaces).isEmpty
        {
            txt_fullName.becomeFirstResponder()
            FXAlertController.alert(AppName, message: EmptyFullName)
            return false
        }
        if txt_email.text!.isEmpty || txt_email.text!.trimmingCharacters(in: whiteSpaces).isEmpty
        {
            txt_email.becomeFirstResponder()
            FXAlertController.alert(AppName, message: EmptyEmail)
            return false
        }
        if txt_password.text!.isEmpty || txt_password.text!.trimmingCharacters(in: whiteSpaces).isEmpty
        {
            txt_password.becomeFirstResponder()
            FXAlertController.alert(AppName, message: EmptyPassword)
            return false
        }
        if txt_confirmPassword.text!.isEmpty || txt_confirmPassword.text!.trimmingCharacters(in: whiteSpaces).isEmpty
        {
            txt_confirmPassword.becomeFirstResponder()
             FXAlertController.alert(AppName, message: EmptyConfirmPassword)
            return false
        }
        if txt_password.text! != txt_confirmPassword.text!
        {
            txt_password.becomeFirstResponder()
            FXAlertController.alert(AppName, message: PasswordMatch)
            return false
        }
        if !FXAlertController.isValidEmail(testStr: txt_email.text!)
        {
            txt_email.becomeFirstResponder()
             FXAlertController.alert(AppName, message: InvalidEmail)
            return false
        }
        if txt_accessCode.text!.isEmpty || txt_accessCode.text!.trimmingCharacters(in: whiteSpaces).isEmpty
        {
            txt_accessCode.becomeFirstResponder()
             FXAlertController.alert(AppName, message: EmptyAccessCode)
            return false
        }
        return true
    }
    
    func apiSihnUp(){
        self.view.endEditing(true)
          if self.areValidInputs() {
          if Reachability.isConnectedToNetwork() {
          let apiURL: String = "\(BASEURL)\(APISignUp)"
          print("\(apiURL)")
            var devicetoken = String()
            if UserDefaults.standard.value(forKey: "DEVICETOKEN") as? String != nil{
                devicetoken = UserDefaults.standard.value(forKey: "DEVICETOKEN") as! String
            }
            else{
                devicetoken = "fqm7qMYw9X4:APA91bGxsAu2mWut0v1icc4Dj9whCOUF4MP5HphwjvTpit3ym7o1_0TnPRKMyLD3XHmPVnRbMEf5cAOkyzX1A8Ox9QLAj1PCfXrHtINFDbeQFqTrQOH2EqZ7_JxovOwFZDuLbUfCsmBy"
            }
            let newTodo: [String: Any] = ["email": txt_email.text ?? "123","password": txt_password.text ?? "123","device_type":DeviceType,"fullname":txt_fullName.text!,"access_code":txt_accessCode.text!,"device_token":devicetoken,"cpass":txt_confirmPassword.text!]
              let size = CGSize(width: 30, height: 30)
          startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
          AF.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in
              switch response.result {
               case .success(let value):
                  print(response)
                  let json = value as? [String: Any]
                 var errorres = false
                 if json!["error"] as? Bool != nil{
                  errorres = json!["error"] as! Bool
                 }
                 if json!["error"] as? String != nil{
                   let error:String = json!["error"] as! String
                   if error == "false"{
                       errorres = false
                   }
                   else{
                       errorres = true
                   }
                 }
                  if !errorres{
                    let alertController = UIAlertController(title: AppName, message: (json!["message"] as! String), preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                  }
                  else{
                    FXAlertController.alert(AppName, message: json!["message"] as! String)
                    }
                  DispatchQueue.main.async {
                    self.stopAnimating(nil)
                  }
                  break
              case .failure(let error):
                  print(error)
                  DispatchQueue.main.async {
                 self.stopAnimating(nil)
                  }
              }
           }
          }
        }
    }

    @IBAction func btn_SignUpAction(_ sender: Any) {
      apiSihnUp()
    }
     @IBAction func btn_SignInAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

//
//  LoginVC.swift
//  Broski
//
//  Created by deepkohli on 23/12/19.
//  Copyright © 2019 Webmobril.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView


class LoginVC: UIViewController,NVActivityIndicatorViewable,UITextFieldDelegate {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var btn_signIn: UIButton!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_password: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInitialValue()
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
    
    }
    
    func setUpInitialValue(){
        btn_signIn.layer.cornerRadius = btn_signIn.frame.size.height / 2
        btn_signIn.clipsToBounds = true
        emailView.dropBorder()
        passwordView.dropBorder()
        mainView.dropShadow()
        //RoundShadowView.layout(mainView)
        //RoundShadowView.layoutView(mainView)
    }
    
    func dataValidation() -> Bool {
        let whiteSpaces = CharacterSet.whitespacesAndNewlines
            if txt_email.text!.isEmpty || txt_email.text!.trimmingCharacters(in: whiteSpaces).isEmpty {
                FXAlertController.alert(AppName, message: EmptyEmail)
                txt_email.becomeFirstResponder()
                return false
            }
            if !FXAlertController.isValidEmail(testStr: txt_email.text!)
            {
                txt_email.becomeFirstResponder()
                 FXAlertController.alert(AppName, message: InvalidEmail)
                return false
                
            }
            if txt_password.text!.isEmpty || txt_password.text!.trimmingCharacters(in: whiteSpaces).isEmpty{
                 FXAlertController.alert(AppName, message: EmptyPassword)
                 txt_password.becomeFirstResponder()
                return false
            }

            return true
        }

    func apiLogin(){
        self.view.endEditing(true)
          if self.dataValidation() {
          if Reachability.isConnectedToNetwork() {
          let apiURL: String = "\(BASEURL)\(APILOgin)"
          print("\(apiURL)")
          var devicetoken = String()
          if UserDefaults.standard.value(forKey: "DEVICETOKEN") as? String != nil{
          devicetoken = UserDefaults.standard.value(forKey: "DEVICETOKEN") as! String
          }
          else{
          devicetoken = "fqm7qMYw9X4:APA91bGxsAu2mWut0v1icc4Dj9whCOUF4MP5HphwjvTpit3ym7o1_0TnPRKMyLD3XHmPVnRbMEf5cAOkyzX1A8Ox9QLAj1PCfXrHtINFDbeQFqTrQOH2EqZ7_JxovOwFZDuLbUfCsmBy"
          }
          let newTodo: [String: Any] = ["email": txt_email.text ?? "123","password": txt_password.text ?? "123","device_type":DeviceType,"device_token": devicetoken]
              let size = CGSize(width: 30, height: 30)
          startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
            
            
          AF.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in
              switch response.result {
               case .success(let value):
                  print(response)
                  
                  let json = value as? [String: Any]
                  
                  var errorres = false
                  if json!["error"] as? Bool != nil{
                   errorres = json!["error"] as! Bool
                  }
                  if json!["error"] as? String != nil{
                    let error:String = json!["error"] as! String
                    if error == "false"{
                        errorres = false
                    }
                    else{
                        errorres = true
                    }
                  }
                  
                  if !errorres{
                      //let message = json!["message"] as! String
                    let resultDict = json!["result"] as? [String: Any]
                    UserDefaults.standard.set(resultDict!["email"] as! String, forKey: "EmailVal")
                    UserDefaults.standard.set(resultDict!["name"] as! String, forKey: "NameVal")
                    UserDefaults.standard.set(resultDict!["device_token"] as! String, forKey: "TokenVal")
                    if resultDict!["id"] as? Int != nil{
                       UserDefaults.standard.set(String(format: "%d", resultDict!["id"] as! Int), forKey: "IdVal")
                    }
                    if resultDict!["id"] as? String != nil{
                       UserDefaults.standard.set(resultDict!["id"] as! String, forKey: "IdVal")
                    }
                    if resultDict!["device_type"] as? Int != nil{
                       UserDefaults.standard.set(String(format: "%d", resultDict!["device_type"] as! Int), forKey: "DeviceTypeVal")
                    }
                    if resultDict!["device_type"] as? String != nil{
                       UserDefaults.standard.set(resultDict!["device_type"] as! String, forKey: "DeviceTypeVal")
                    }
                    let alertController = UIAlertController(title: AppName, message: (json!["message"] as! String), preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        UserDefaults.standard.set(true, forKey: "IsLogin")
                        appDelegate.homeView()
                       //let vc = Router.sharedInst().getHomeView()
                      // self.navigationController?.pushViewController(vc, animated: true)
                        
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                  }
                  else{
                     FXAlertController.alert(AppName, message: json!["message"] as! String)
                  }
                  DispatchQueue.main.async {
                    self.stopAnimating(nil)
                  }
                  break
                  
              case .failure(let error):
                  print(error)
                  DispatchQueue.main.async {
                 self.stopAnimating(nil)
                  }
              }
           }
          }
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    @IBAction func btn_SignInAction(_ sender: Any) {
        apiLogin()
        //let vc = Router.sharedInst().getHomeView()
        //self.navigationController?.pushViewController(vc, animated: true)
        //appDelegate.homeView()
    }
    
    @IBAction func btn_ForgotPasswordAction(_ sender: Any) {
        let vc = Router.sharedInst().getForgotPasswordView()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btn_SignUpAction(_ sender: Any) {
       let vc = Router.sharedInst().getSignUpView()
       self.navigationController?.pushViewController(vc, animated: true)
    }
}

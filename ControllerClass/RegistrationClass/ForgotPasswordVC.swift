//
//  ForgotPasswordVC.swift
//  Broski
//
//  Created by deepkohli on 23/12/19.
//  Copyright © 2019 Webmobril.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class ForgotPasswordVC: UIViewController,NVActivityIndicatorViewable {
    
    @IBOutlet weak var sucessMsgView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var btn_resetPassword: UIButton!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_VerifyOTP: UITextField!
    @IBOutlet weak var verifyOTPView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInitialValue()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        sucessMsgView.isHidden = true
    }

    func setUpInitialValue(){
        btn_submit.layer.cornerRadius = btn_submit.frame.size.height / 2
        btn_submit.clipsToBounds = true
        btn_resetPassword.layer.cornerRadius = btn_resetPassword.frame.size.height / 2
        btn_resetPassword.clipsToBounds = true
        emailView.dropBorder()
        verifyOTPView.dropBorder()
        mainView.dropShadow()
    }
    func dataValidation() -> Bool {
    let whiteSpaces = CharacterSet.whitespacesAndNewlines
        if txt_email.text!.isEmpty || txt_email.text!.trimmingCharacters(in: whiteSpaces).isEmpty {
            FXAlertController.alert(AppName, message: EmptyEmail)
            txt_email.becomeFirstResponder()
            return false
        }
        if !FXAlertController.isValidEmail(testStr: txt_email.text!)
        {
            txt_email.becomeFirstResponder()
             FXAlertController.alert(AppName, message: InvalidEmail)
            return false
            
        }
       
        return true
    }
    func oTPValidation() -> Bool {
    let whiteSpaces = CharacterSet.whitespacesAndNewlines
        if txt_VerifyOTP.text!.isEmpty || txt_VerifyOTP.text!.trimmingCharacters(in: whiteSpaces).isEmpty {
            FXAlertController.alert(AppName, message: EmptyOTPField)
            txt_email.becomeFirstResponder()
            return false
        }
        return true
    }
    
    func apiResendOTP(){
        self.view.endEditing(true)
            //if self.oTPValidation() {
          if Reachability.isConnectedToNetwork() {
          let apiURL: String = "\(BASEURL)\(RESENDOTP)"
          print("\(apiURL)")
            var devicetoken = String()
            if UserDefaults.standard.value(forKey: "DEVICETOKEN") as? String != nil{
            devicetoken = UserDefaults.standard.value(forKey: "DEVICETOKEN") as! String
            }
            else{
            devicetoken = "fqm7qMYw9X4:APA91bGxsAu2mWut0v1icc4Dj9whCOUF4MP5HphwjvTpit3ym7o1_0TnPRKMyLD3XHmPVnRbMEf5cAOkyzX1A8Ox9QLAj1PCfXrHtINFDbeQFqTrQOH2EqZ7_JxovOwFZDuLbUfCsmBy"
            }
            let newTodo: [String: Any] = ["email": txt_email.text!,"device_type": DeviceType,"device_token": devicetoken]
              let size = CGSize(width: 30, height: 30)
            // let headers: HTTPHeaders = ["Content-Type": "application/json"]
          startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
           // AF.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.default, headers: headers).responseJSON{ response in

          AF.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in
              switch response.result {
               case .success(let value):
                  print(response)
                  
                  let json = value as? [String: Any]
                  
                 var errorres = false
                 if json!["error"] as? Bool != nil{
                  errorres = json!["error"] as! Bool
                 }
                 if json!["error"] as? String != nil{
                   let error:String = json!["error"] as! String
                   if error == "false"{
                       errorres = false
                   }
                   else{
                       errorres = true
                   }
                 }
                  
                  if !errorres {
                     let alertController = UIAlertController(title: AppName, message: (json!["message"] as! String), preferredStyle: .alert)
                     let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                         UIAlertAction in
                        //let vc = Router.sharedInst().getresetPasswordView()
                        //self.navigationController?.pushViewController(vc, animated: true)
                     }
                     alertController.addAction(okAction)
                     self.present(alertController, animated: true, completion: nil)
                  }
                  else{
                    FXAlertController.alert(AppName, message: json!["message"] as! String)
                  }
                  
                  DispatchQueue.main.async {
                    self.stopAnimating(nil)
                  }
                  break
                  
              case .failure(let error):
                  print(error)
                  DispatchQueue.main.async {
                 self.stopAnimating(nil)
                  }
              }
           }
          }
        //}
    }
    
    func apiVeriFyOTP(){
        self.view.endEditing(true)
          if self.oTPValidation() {
          if Reachability.isConnectedToNetwork() {
          let apiURL: String = "\(BASEURL)\(VERIFYOTP)"
          print("\(apiURL)")
            var devicetoken = String()
            if UserDefaults.standard.value(forKey: "DEVICETOKEN") as? String != nil{
            devicetoken = UserDefaults.standard.value(forKey: "DEVICETOKEN") as! String
            }
            else{
            devicetoken = "fqm7qMYw9X4:APA91bGxsAu2mWut0v1icc4Dj9whCOUF4MP5HphwjvTpit3ym7o1_0TnPRKMyLD3XHmPVnRbMEf5cAOkyzX1A8Ox9QLAj1PCfXrHtINFDbeQFqTrQOH2EqZ7_JxovOwFZDuLbUfCsmBy"
            }
            let newTodo: [String: Any] = ["email": txt_email.text!,"otp":txt_VerifyOTP.text!]
              let size = CGSize(width: 30, height: 30)
            // let headers: HTTPHeaders = ["Content-Type": "application/json"]
          startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
           // AF.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.default, headers: headers).responseJSON{ response in

          AF.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in
              switch response.result {
               case .success(let value):
                  print(response)
                  
                  let json = value as? [String: Any]
                  
                 var errorres = false
                 if json!["error"] as? Bool != nil{
                  errorres = json!["error"] as! Bool
                 }
                 if json!["error"] as? String != nil{
                   let error:String = json!["error"] as! String
                   if error == "false"{
                       errorres = false
                   }
                   else{
                       errorres = true
                   }
                 }
                  
                  if !errorres {
                     let alertController = UIAlertController(title: AppName, message: (json!["message"] as! String), preferredStyle: .alert)
                     let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                         UIAlertAction in
                        let vc = Router.sharedInst().getresetPasswordView()
                        vc.strEmailId = self.txt_email.text!
                        self.navigationController?.pushViewController(vc, animated: true)
                     }
                     alertController.addAction(okAction)
                     self.present(alertController, animated: true, completion: nil)
                  }
                  else{
                    FXAlertController.alert(AppName, message: json!["message"] as! String)
                  }
                  
                  DispatchQueue.main.async {
                    self.stopAnimating(nil)
                  }
                  break
                  
              case .failure(let error):
                  print(error)
                  DispatchQueue.main.async {
                 self.stopAnimating(nil)
                  }
              }
           }
          }
        }
    }
    
    func apiForgotPassword(){
        self.view.endEditing(true)
          if self.dataValidation() {
          if Reachability.isConnectedToNetwork() {
          let apiURL: String = "\(BASEURL)\(APIForgotPassword)"
          print("\(apiURL)")
            var devicetoken = String()
            if UserDefaults.standard.value(forKey: "DEVICETOKEN") as? String != nil{
            devicetoken = UserDefaults.standard.value(forKey: "DEVICETOKEN") as! String
            }
            else{
            devicetoken = "fqm7qMYw9X4:APA91bGxsAu2mWut0v1icc4Dj9whCOUF4MP5HphwjvTpit3ym7o1_0TnPRKMyLD3XHmPVnRbMEf5cAOkyzX1A8Ox9QLAj1PCfXrHtINFDbeQFqTrQOH2EqZ7_JxovOwFZDuLbUfCsmBy"
            }
            let newTodo: [String: Any] = ["email": txt_email.text!,"device_type": DeviceType,"device_token": devicetoken]
              let size = CGSize(width: 30, height: 30)
            // let headers: HTTPHeaders = ["Content-Type": "application/json"]
          startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
           // AF.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.default, headers: headers).responseJSON{ response in

          AF.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in
              switch response.result {
               case .success(let value):
                  print(response)
                  
                  let json = value as? [String: Any]
                  
                 var errorres = false
                 if json!["error"] as? Bool != nil{
                  errorres = json!["error"] as! Bool
                 }
                 if json!["error"] as? String != nil{
                   let error:String = json!["error"] as! String
                   if error == "false"{
                       errorres = false
                   }
                   else{
                       errorres = true
                   }
                 }
                  
                  if !errorres {
                     //let alertController = UIAlertController(title: AppName, message: (json!["message"] as! String), preferredStyle: .alert)
                     //let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                       //  UIAlertAction in
                        //let vc = Router.sharedInst().getresetPasswordView()
                        //self.navigationController?.pushViewController(vc, animated: true)
                        self.sucessMsgView.isHidden = false
                     //}
                     //alertController.addAction(okAction)
                     //self.present(alertController, animated: true, completion: nil)
                  }
                  else{
                    FXAlertController.alert(AppName, message: json!["message"] as! String)
                  }
                  
                  DispatchQueue.main.async {
                    self.stopAnimating(nil)
                  }
                  break
                  
              case .failure(let error):
                  print(error)
                  DispatchQueue.main.async {
                 self.stopAnimating(nil)
                  }
              }
           }
          }
        }
    }
    @IBAction func btn_SubmitAction(_ sender: Any) {
        apiForgotPassword()
    }
    @IBAction func btn_ResetPasswordAction(_ sender: Any) {
        apiVeriFyOTP()
    }
    @IBAction func btn_ResendOTPAction(_ sender: Any) {
        apiResendOTP()
    }
    @IBAction func btn_BackAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }

}

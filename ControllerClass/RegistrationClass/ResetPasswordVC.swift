//
//  ResetPasswordVC.swift
//  Broski
//
//  Created by deepkohli on 23/12/19.
//  Copyright © 2019 Webmobril.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class ResetPasswordVC: UIViewController,NVActivityIndicatorViewable {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var btn_reset: UIButton!
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var newPasswordView: UIView!
    @IBOutlet weak var txt_newPassword: UITextField!
    @IBOutlet weak var txt_confirmPassword: UITextField!
    
    var strEmailId = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInitialValue()

        // Do any additional setup after loading the view.
    }

    func setUpInitialValue(){
        btn_reset.layer.cornerRadius = btn_reset.frame.size.height / 2
        btn_reset.clipsToBounds = true
        confirmPasswordView.dropBorder()
        newPasswordView.dropBorder()
        mainView.dropShadow()
    }
    func dataValidation() -> Bool {
    let whiteSpaces = CharacterSet.whitespacesAndNewlines
        if txt_newPassword.text!.isEmpty || txt_newPassword.text!.trimmingCharacters(in: whiteSpaces).isEmpty {
            FXAlertController.alert(AppName, message: NewPassword)
            txt_newPassword.becomeFirstResponder()
            return false
        }
        if txt_confirmPassword.text!.isEmpty || txt_confirmPassword.text!.trimmingCharacters(in: whiteSpaces).isEmpty {
            FXAlertController.alert(AppName, message: EmptyConfirmPassword)
            txt_newPassword.becomeFirstResponder()
            return false
        }
       if txt_newPassword.text! != txt_confirmPassword.text!
       {
           txt_newPassword.becomeFirstResponder()
           FXAlertController.alert(AppName, message: PasswordMatch)
           return false
           
       }
        return true
    }
    func apiResetPassword(){
        self.view.endEditing(true)
          if self.dataValidation() {
          if Reachability.isConnectedToNetwork() {
          let apiURL: String = "\(BASEURL)\(APIResetPassword)"
          print("\(apiURL)")
            var devicetoken = String()
            if UserDefaults.standard.value(forKey: "DEVICETOKEN") as? String != nil{
            devicetoken = UserDefaults.standard.value(forKey: "DEVICETOKEN") as! String
            }
            else{
            devicetoken = "fqm7qMYw9X4:APA91bGxsAu2mWut0v1icc4Dj9whCOUF4MP5HphwjvTpit3ym7o1_0TnPRKMyLD3XHmPVnRbMEf5cAOkyzX1A8Ox9QLAj1PCfXrHtINFDbeQFqTrQOH2EqZ7_JxovOwFZDuLbUfCsmBy"
            }
          let newTodo: [String: Any] = ["newpass": txt_newPassword.text ?? "123","email": strEmailId,"cpass": txt_confirmPassword.text!,"device_type":DeviceType,"device_token": devicetoken]
              let size = CGSize(width: 30, height: 30)
          startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
          AF.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in
              switch response.result {
               case .success(let value):
                  print(response)
                  
                  let json = value as? [String: Any]
                 
                  var errorres = false
                  if json!["error"] as? Bool != nil{
                   errorres = json!["error"] as! Bool
                  }
                  if json!["error"] as? String != nil{
                    let error:String = json!["error"] as! String
                    if error == "false"{
                        errorres = false
                    }
                    else{
                        errorres = true
                    }
                  }
                   
                   if !errorres {
                      let alertController = UIAlertController(title: AppName, message: (json!["message"] as! String), preferredStyle: .alert)
                      let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                          UIAlertAction in
                        self.navigationController?.popToRootViewController(animated: true)
                      }
                      alertController.addAction(okAction)
                      self.present(alertController, animated: true, completion: nil)
                   }
                   else{
                     FXAlertController.alert(AppName, message: json!["message"] as! String)
                   }
                  
                  DispatchQueue.main.async {
                    self.stopAnimating(nil)
                  }
                  break
                  
              case .failure(let error):
                  print(error)
                  DispatchQueue.main.async {
                 self.stopAnimating(nil)
                  }
              }
           }
          }
        }
    }
    @IBAction func btn_resetPasswordAction(_ sender: Any) {
        apiResetPassword()
    }
    @IBAction func btn_BackAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }

}

//
//  PowerOptionVC.swift
//  Broski
//
//  Created by deepkohli on 31/12/19.
//  Copyright © 2019 Webmobril.com. All rights reserved.
//

import UIKit

class PowerOptionVC: UIViewController {
    
    @IBOutlet weak var btnEqualizer: UIButton!
    @IBOutlet weak var btnBassBoost: UIButton!
    @IBOutlet weak var btnSound: UIButton!
    @IBOutlet weak var btnAmplifire: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
    }
    @IBAction func action_Equalizer(_ sender: Any) {
        if (btnEqualizer.isSelected == true)
        {
            btnEqualizer.setImage(UIImage(named: "check22"), for: .normal)
            btnEqualizer.isSelected = false
        }
        else
        {
            btnEqualizer.setImage(UIImage(named: "check11"), for: .normal)
            btnEqualizer.isSelected = true
        }
    }
    @IBAction func action_BassBoost(_ sender: Any) {
        if (btnBassBoost.isSelected == true)
        {
            btnBassBoost.setImage(UIImage(named: "check22"), for: .normal)
            btnBassBoost.isSelected = false
        }
        else
        {
            btnBassBoost.setImage(UIImage(named: "check11"), for: .normal)
            btnBassBoost.isSelected = true
        }
    }
    @IBAction func action_Sound(_ sender: Any) {
        if (btnSound.isSelected == true)
        {
            btnSound.setImage(UIImage(named: "check22"), for: .normal)
            btnSound.isSelected = false
        }
        else
        {
            btnSound.setImage(UIImage(named: "check11"), for: .normal)
            btnSound.isSelected = true
        }
    }
    @IBAction func action_Amplifire(_ sender: Any) {
        if (btnAmplifire.isSelected == true)
        {
            btnAmplifire.setImage(UIImage(named: "check22"), for: .normal)
            btnAmplifire.isSelected = false
        }
        else
        {
            btnAmplifire.setImage(UIImage(named: "check11"), for: .normal)
            btnAmplifire.isSelected = true
        }
    }
    
     @IBAction func action_Cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
     @IBAction func action_OK(_ sender: Any) {
    }
    

}

//
//  ITunesPlayerVC.swift
//  Broski
//
//  Created by deepkohli on 03/02/20.
//  Copyright © 2020 Webmobril.com. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation

class ITunesPlayerVC: UIViewController,MPMediaPickerControllerDelegate {
    
    var mediaPicker: MPMediaPickerController?
    var myMusicPlayer: MPMusicPlayerController?
    let masterVolumeSlider: MPVolumeView = MPVolumeView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        displayMediaPickerAndPlayItem()
     // Do any additional setup after loading the view.
    }
    
    func updateNowPlayingItem(){
        if let nowPlayingItem=self.myMusicPlayer!.nowPlayingItem{
            let nowPlayingTitle = nowPlayingItem.title
           // self.nowPlayingLabel.text=nowPlayingTitle
        }else{
            //self.nowPlayingLabel.text="Nothing Played"
        }
    }
    
    func mediaPicker(mediaPicker: MPMediaPickerController,
        didPickMediaItems mediaItemCollection: MPMediaItemCollection){
            if let player = myMusicPlayer{
                player.beginGeneratingPlaybackNotifications()
                
                player.setQueue(with: mediaItemCollection)
                player.play()
                self.updateNowPlayingItem()
                mediaPicker.dismiss(animated: true, completion: nil)
            }
    }
    func displayMediaPickerAndPlayItem(){
        mediaPicker = MPMediaPickerController(mediaTypes: .anyAudio)
        if let picker = mediaPicker{
            print("Successfully instantiated a media picker")
            picker.delegate = self
            view.addSubview(picker.view)
            present(picker, animated: true, completion: nil)
        } else {
            print("Could not instantiate a media picker")
        }
    }
    func nowPlayingItemIsChanged(notification: NSNotification){
        print("Playing Item Is Changed")
        let key = "MPMusicPlayerControllerNowPlayingItemPersistentIDKey"
        let persistentID =
        notification.userInfo![key] as? NSString
        if let id = persistentID{
            print("Persistent ID = \(id)")
        }
    }
    func volumeIsChanged(notification: NSNotification){
        print("Volume Is Changed")
    }
    @IBAction func openItunesLibraryTapped(sender: AnyObject) {
        displayMediaPickerAndPlayItem()
    }
    
    @IBAction func sliderVolume(sender: AnyObject) {
        if let view = masterVolumeSlider.subviews.first as? UISlider{
            view.value = sender.value
        }
    }

}

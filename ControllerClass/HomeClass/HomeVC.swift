//
//  HomeVC.swift
//  Broski
//
//  Created by deepkohli on 23/12/19.
//  Copyright © 2019 Webmobril.com. All rights reserved.
//

import UIKit
import AVFoundation
import StoreKit
import MediaPlayer

class HomeVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var requestBtn: UIButton!
    @IBOutlet weak var viewForPlayer: UIView!
    
    let viewTransitionDelegate = TransitionDelegate()
    
   var arrayForIcon = ["jazz","classic","folk","flat","pop","dance","hip-hop"]
   var arrayForName = ["Jazz","Classic","Folk","Flat","Pop","Dance","Hip Hop"]
    
    var player: AVAudioPlayer?
    var selectedIndexNumber = Int()
    
     var appleMusicManager = AppleMusicManager()
    lazy var tempauthorizationCheckManager: AuthorizationManager = {
               return AuthorizationManager(appleMusicManager: self.appleMusicManager)
           }()
    
    var authorizationManager: AuthorizationManager!
    var authorizationDataSource: AuthorizationDataSource!
    var didPresentCloudServiceSetup = false
    
    override func viewDidLoad() {
        viewForPlayer.isHidden = true
        collectionView.isHidden = true
        super.viewDidLoad()
        selectedIndexNumber = 10001
        UserDefaults.standard.set("jazz", forKey: "SoundType")
        setiTuneAuthenticationData()
    }
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           setAuthorizationRequestButtonState()
       }
    deinit {
        // Remove all notification observers.
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.removeObserver(self,
                                          name: AuthorizationManager.cloudServiceDidUpdateNotification,
                                          object: nil)
        notificationCenter.removeObserver(self,
                                          name: AuthorizationManager.authorizationDidUpdateNotification,
                                          object: nil)
        notificationCenter.removeObserver(self,
                                          name: UIApplication.willEnterForegroundNotification,
                                          object: nil)
    }
    // MARK: UI Updating Methods
    func setAuthorizationRequestButtonState() {
        if SKCloudServiceController.authorizationStatus() == .notDetermined || MPMediaLibrary.authorizationStatus() == .notDetermined {
            requestBtn.isEnabled = true
            requestBtn.setTitle("Request", for: .normal)
        } else {
            requestBtn.isEnabled = false
            requestBtn.setTitle("Authorized", for: .normal)
        }
    }
    
    func setiTuneAuthenticationData(){
       
        authorizationManager = tempauthorizationCheckManager
        
        authorizationDataSource = AuthorizationDataSource(authorizationManager: authorizationManager)
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.addObserver(self,
                                       selector: #selector(handleAuthorizationManagerDidUpdateNotification),
                                       name: AuthorizationManager.cloudServiceDidUpdateNotification,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(handleAuthorizationManagerDidUpdateNotification),
                                       name: AuthorizationManager.authorizationDidUpdateNotification,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(handleAuthorizationManagerDidUpdateNotification),
                                       name: UIApplication.willEnterForegroundNotification,
                                       object: nil)
        
        setAuthorizationRequestButtonState()
    }
    
    // MARK: Notification Observing Methods
        
    @objc func handleAuthorizationManagerDidUpdateNotification() {
            DispatchQueue.main.async {
                if SKCloudServiceController.authorizationStatus() == .notDetermined || MPMediaLibrary.authorizationStatus() == .notDetermined {
                    self.navigationItem.rightBarButtonItem?.isEnabled = true
                } else {
                    self.navigationItem.rightBarButtonItem?.isEnabled = false
                    
                    if self.authorizationManager.cloudServiceCapabilities.contains(.musicCatalogSubscriptionEligible) &&
                        !self.authorizationManager.cloudServiceCapabilities.contains(.musicCatalogPlayback) {
                        self.presentCloudServiceSetup()
                    }
                    
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    

     @IBAction func action_authorize(_ sender: Any) {
        authorizationManager.requestCloudServiceAuthorization()
        authorizationManager.requestMediaLibraryAuthorization()
    }
     @IBAction func action_Playlist(_ sender: Any) {
    }
     @IBAction func action_Play(_ sender: Any) {
    }
     @IBAction func action_Recent(_ sender: Any) {
    }
     @IBAction func action_Search(_ sender: Any) {
        let vc = Router.sharedInst().getMediaSearchTableView()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func action_settings(_ sender: Any) {
     let vc = Router.sharedInst().getAccountSettingsView()
     self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
    func displaySpotifyPlaymusic(){
        let vc = Router.sharedInst().getSpotifyPlayMusicView()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func displaySpotifyPopUp(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SpotifyVCPopUp") as! SpotifyVCPopUp
        vc.modalPresentationStyle = .custom
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .custom
        nav.transitioningDelegate = self.viewTransitionDelegate
        _ = nav.popoverPresentationController
        vc.preferredContentSize = CGSize(width: 500,height: 600)
        self.present(nav, animated: true, completion: nil)
    }
    func displayPowerOptionPopUp(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PowerOptionVC") as! PowerOptionVC
        vc.modalPresentationStyle = .custom
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .custom
        nav.transitioningDelegate = self.viewTransitionDelegate
        _ = nav.popoverPresentationController
        vc.preferredContentSize = CGSize(width: 500,height: 600)
        self.present(nav, animated: true, completion: nil)
    }
    func playSound() {
        let path = Bundle.main.path(forResource: "example.mp3", ofType:nil)!
        let url = URL(fileURLWithPath: path)

        do {
            player = try AVAudioPlayer(contentsOf: url)
            player?.play()
        } catch {
            // couldn't load file :(
        }
    }
    
     
    @IBAction func action_headPhone(_ sender: Any) {
           
       }
    @IBAction func action_power(_ sender: Any) {
           displayPowerOptionPopUp()
       }
 */
    func getiTunePlayerVC(){
        let vc = Router.sharedInst().getiTunePlayerView()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension HomeVC: UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return authorizationDataSource.numberOfSections()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return authorizationDataSource.numberOfItems(in: section)
    }
    
     func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return authorizationDataSource.sectionTitle(for: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AuthorizationCellIdentifier", for: indexPath)
        
        
        cell.textLabel?.text = authorizationDataSource.stringForItem(at: indexPath)
        if indexPath.section == 0{
            if indexPath.row == 0{
                if cell.textLabel?.text == "Authorized"{
                    viewForPlayer.isHidden = false
                    requestBtn.setTitle("Authorized", for: .normal)
                }
            }
        }
        return cell
    }
    
    // MARK: SKCloudServiceSetupViewController Method
    
    func presentCloudServiceSetup() {
        
        guard didPresentCloudServiceSetup == false else {
            return
        }
        
        /*
         If the current `SKCloudServiceCapability` includes `.musicCatalogSubscriptionEligible`, this means that the currently signed in iTunes Store
         account is elgible for an Apple Music Trial Subscription.  To provide the user with an option to sign up for a free trial, your application
         can present the `SKCloudServiceSetupViewController` as demonstrated below.
        */
        
        let cloudServiceSetupViewController = SKCloudServiceSetupViewController()
        cloudServiceSetupViewController.delegate = self
        
        cloudServiceSetupViewController.load(options: [.action: SKCloudServiceSetupAction.subscribe]) { [weak self] (result, error) in
            guard error == nil else {
                fatalError("An Error occurred: \(error!.localizedDescription)")
            }
            
            if result {
                self?.present(cloudServiceSetupViewController, animated: true, completion: nil)
                self?.didPresentCloudServiceSetup = true
            }
        }
    }
}

extension HomeVC: SKCloudServiceSetupViewControllerDelegate {
    func cloudServiceSetupViewControllerDidDismiss(_ cloudServiceSetupViewController: SKCloudServiceSetupViewController) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

extension HomeVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           return arrayForIcon.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell: MusicCategoryCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MusicCategoryCollectionCell", for: indexPath) as! MusicCategoryCollectionCell
            
              cell.categoryIcon.image = UIImage(named:arrayForIcon[indexPath.item] )
              cell.categoryName.text = arrayForName[indexPath.item]
            
            if selectedIndexNumber == indexPath.row {
                cell.checkIcon.isHidden = false
            }
            else{
                cell.checkIcon.isHidden = true
            }
            
            // var imageURL = (arr_allProduct_category[indexPath.item] as! NSDictionary).object(forKey: "image") as? String ?? ""
//             if imageURL == "" {
//                 cell.product_category_Imagview.image = #imageLiteral(resourceName: "Thumbnail")
//             } else {
//                 imageURL = appConstant.imageURL + imageURL
                // cell.categoryIcon.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "Thumbnail"))

            // }
            return cell
        }
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            UserDefaults.standard.set(arrayForName[indexPath.item], forKey: "SoundType")
            selectedIndexNumber = indexPath.row
            collectionView.reloadData()
            //displaySpotifyPopUp()
            getiTunePlayerVC()
            
            //displaySpotifyPlaymusic()
           // playSound()
            
           // let vc = Router.sharedInst().getSpotifyLoginView()
           // self.navigationController?.pushViewController(vc, animated: true)
            
        }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var padding = CGFloat()
        if self.view.frame.size.width <= 375{
            padding =  60
        }
        else{
            padding =  100
        }
        
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
    }
    
}

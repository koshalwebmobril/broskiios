//
//  MusicTrackListVC.swift
//  Broski
//
//  Created by deepkohli on 21/01/20.
//  Copyright © 2020 Webmobril.com. All rights reserved.
//

import UIKit
@_exported import Alamofire
import AVFoundation
import SVProgressHUD
import NVActivityIndicatorView
import SwiftyJSON

 
class MusicTrackListVC: UIViewController,NVActivityIndicatorViewable {
    
    var userIdVal = String()
    var strAccessToken = String()
    typealias JSONStandard = [String : AnyObject]


    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        callAlamofire()

    }
    
    func callAlamofire(){
    if Reachability.isConnectedToNetwork() {
        
        let headers: HTTPHeaders = [
        "Authorization":  String(format: "%@%@","Bearer ",strAccessToken),
        "Content-Type": "application/json","Accept": "application/json"]
        
        let size = CGSize(width: 30, height: 30)
        self.startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
         //let playListURLString = "https://api.spotify.com/v1/users?user_id=\(userIdVal)/playlists"
        let playListURLString = "https://api.spotify.com/v1/users/\(userIdVal)/playlists"
    
    print(playListURLString)
    AF.request(playListURLString, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON {
                        response in
                        switch response.result {
                         case .success(let value):
                            print(response)
                            
                            let json = value as? [String: Any]
                            
                            if json!["items"] as? [String: Any] != nil{
                                
                            }
                            
//                            let errorres = json!["error"] as? Bool
//
//                            if errorres {
//                                FXAlertController.alert(AppName, message: json!["message"] as! String)
//                            }
//                            else{
//                                let resultDict = json!["result"] as? [String: Any]
//
//
//                            }
                            
                            DispatchQueue.main.async {
                              self.stopAnimating(nil)
                            }
                            break
                            
                        case .failure(let error):
                            print(error)
                            DispatchQueue.main.async {
                           self.stopAnimating(nil)
                            }
                        }//
                    }
                }
    }
    
    
    
    @IBAction func backButtonAction(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

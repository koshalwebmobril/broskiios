//
//  SpotifyLoginVC.swift
//  Broski
//
//  Created by deepkohli on 13/01/20.
//  Copyright © 2020 Webmobril.com. All rights reserved.
//

import UIKit
import SafariServices
import Alamofire

class SpotifyLoginVC: UIViewController,SPTAudioStreamingPlaybackDelegate, SPTAudioStreamingDelegate {
    
    @IBOutlet weak var searchButtn : UIButton!
    @IBOutlet weak var spotifyButton : UIButton!
    
    var auth = SPTAuth.defaultInstance()!
    var session:SPTSession!
    var player: SPTAudioStreamingController?
    var loginUrl: URL?
    var myplaylists = [SPTPartialPlaylist]()
    var strSpotifyUserId = String()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setup ()
        NotificationCenter.default.addObserver(self, selector: #selector(SpotifyLoginVC.updateAfterFirstLogin), name: NSNotification.Name(rawValue: "loginSuccessfull"), object: nil)

    }
    func setup () {
        // insert redirect your url and client ID below
        let redirectURL = "Broski://returnAfterLogin" // put your redirect URL here
       // let clientID = "b875b386c1cf416db21057cb794083e8" // put your client ID here
        auth.redirectURL     = URL(string: redirectURL)
        auth.clientID        = "b875b386c1cf416db21057cb794083e8"
        auth.requestedScopes = [SPTAuthStreamingScope, SPTAuthPlaylistReadPrivateScope, SPTAuthPlaylistModifyPublicScope, SPTAuthPlaylistModifyPrivateScope]
        loginUrl = auth.spotifyWebAuthenticationURL()
    
        if UserDefaults.standard.string(forKey: "SpotifyLogin") != nil{
            let str = UserDefaults.standard.string(forKey: "SpotifyLogin")
            if str == "SpotifyLoginSuccess"{
                 searchButtn.alpha = 1
                spotifyButton.isHidden = true
            }
            else{
                searchButtn.alpha = 0
                spotifyButton.isHidden = false
            }
        }
        else{
            searchButtn.alpha = 0
        }
        //https://api.spotify.com/v1/tracks/zh0bxrk8o2yingr5jq2b7mypk
    }
    func initializaPlayer(authSession:SPTSession){
        if self.player == nil {
            self.player = SPTAudioStreamingController.sharedInstance()
            self.player!.playbackDelegate = self
            self.player!.delegate = self
            try! player?.start(withClientId: auth.clientID)
            self.player!.login(withAccessToken: authSession.accessToken)
            
        }
        
    }
     @objc func updateAfterFirstLogin () {
        
        UserDefaults.standard.set("SpotifyLoginSuccess", forKey: "SpotifyLogin")
        UserDefaults.standard.synchronize()
            
            spotifyButton.isHidden = true
            searchButtn.alpha = 1
            let userDefaults = UserDefaults.standard
            
            if let sessionObj:AnyObject = userDefaults.object(forKey: "SpotifySession") as AnyObject? {
                
                let sessionDataObj = sessionObj as! Data
                let firstTimeSession = NSKeyedUnarchiver.unarchiveObject(with: sessionDataObj) as! SPTSession
                
                self.session = firstTimeSession
            //   initializaPlayer(authSession: session)
                self.spotifyButton.isHidden = true
                AuthService.instance.sessiontokenId = session.accessToken!
                print(AuthService.instance.sessiontokenId!)
                SPTUser.requestCurrentUser(withAccessToken: session.accessToken) { (error, data) in
                    guard let user = data as? SPTUser else { print("Couldn't cast as SPTUser"); return }
                    AuthService.instance.sessionuserId = user.canonicalUserName
                     self.strSpotifyUserId = user.canonicalUserName
                    print(AuthService.instance.sessionuserId!)
                    
                }
                // Method 1 : To get current user's playlist
                SPTPlaylistList.playlists(forUser: session.canonicalUsername, withAccessToken: session.accessToken, callback: { (error, response) in
                    if let listPage = response as? SPTPlaylistList, let playlists = listPage.items as? [SPTPartialPlaylist] {
                        print(playlists)   // or however you want to parse these
            
                        //  self.myplaylists = playlists
                        self.myplaylists.append(contentsOf: playlists)
                        print(self.myplaylists)
                    }
                })
                // Method 2 : To get current user's playlist
                let playListRequest = try! SPTPlaylistList.createRequestForGettingPlaylists(forUser: AuthService.instance.sessionuserId ?? "", withAccessToken: AuthService.instance.sessiontokenId ?? "")
                AF.request(playListRequest)
                    .response { response in
                        if response.response != nil{
                        let list = try! SPTPlaylistList(from: response.data, with: response.response)
                        if list.items != nil{
                        for playList in list.items  {
                            if let playlist = playList as? SPTPartialPlaylist {
                                print( playlist.name! ) // playlist name
                                print( playlist.uri!)    // playlist uri
                                }}
                        }
                        }
                }
                
            }
            
        }
        
        func audioStreamingDidLogin(_ audioStreaming: SPTAudioStreamingController!) {
            // after a user authenticates a session, the SPTAudioStreamingController is then initialized and this method called
            print("logged in")
            SPTUser.requestCurrentUser(withAccessToken: session.accessToken) { (error, data) in
                guard let user = data as? SPTUser else { print("Couldn't cast as SPTUser"); return }
                self.strSpotifyUserId = user.canonicalUserName
                AuthService.instance.sessionuserId = user.canonicalUserName
                print(AuthService.instance.sessionuserId!)
            }
            
    //                self.player?.playSpotifyURI("spotify:track:60a0Rd6pjrkxjPbaKzXjfq", startingWith: 1, startingWithPosition: 8, callback: { (error) in
    //                    if (error != nil) {
    //                        print("playing!")
    //                    }
    //                })
            
            //http://broski.com/callback/
                    
                    
    }
        @IBAction func tableViewTapped(_ sender: Any) {
            
            //     UIApplication.shared.open(loginUrl!, options: nil, completionHandler: nil)
            
            if UIApplication.shared.openURL(loginUrl!) {
                
                if auth.canHandle(auth.redirectURL) {
                    // To do - build in error handling
                }
            }

        }
      
        @IBAction func searchSpotifyBTapped(_ sender: Any) {
            //let vc = Router.sharedInst().getMusicTrackListView()
            //vc.userIdVal = strSpotifyUserId
            //vc.strAccessToken = AuthService.instance.sessiontokenId!
            //self.navigationController?.pushViewController(vc, animated: true)
            let vc = Router.sharedInst().getTableControllerView()
            self.navigationController?.pushViewController(vc, animated: true)
        }
     @IBAction func backBtnTap(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}
extension SpotifyLoginVC {

func showToast(message : String) {
    
    let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 140 , y: self.view.frame.size.height - 100, width: 280, height: 50))
    toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    toastLabel.textColor = UIColor.white
    toastLabel.textAlignment = .center;
    toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
    toastLabel.text = message
    toastLabel.alpha = 1.0
    toastLabel.layer.cornerRadius = 10;
    toastLabel.clipsToBounds  =  true
    self.view.addSubview(toastLabel)
    UIView.animate(withDuration: 10.0, delay: 0.1, options: .curveEaseOut, animations: {
        toastLabel.alpha = 0.0
    }, completion: {(isCompleted) in
        toastLabel.removeFromSuperview()
    })
} }

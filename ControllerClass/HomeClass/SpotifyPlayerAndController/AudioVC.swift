//
//  AudioVC.swift
//  Broski
//
//  Created by deepkohli on 13/01/20.
//  Copyright © 2020 Webmobril.com. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class AudioVC : UIViewController, SPTAudioStreamingPlaybackDelegate, SPTAudioStreamingDelegate,AVAudioPlayerDelegate {
 
    var player = AVAudioPlayer()
    var image = UIImage()
    var mainSongTitle = String()
    var mainPreviewURL = String()
    var tryStrUri = String()
    @IBOutlet var playpausebtn: UIButton!
    @IBOutlet var background: UIImageView!
    @IBOutlet var mainImageView: UIImageView!
    @IBOutlet var songTitle: UILabel!
    
    var strSoundType = String()
    
    override func viewDidLoad() {
        
       self.navigationController?.isNavigationBarHidden = true
        //SoundType
        if UserDefaults.standard.string(forKey: "SoundType") != nil {
            strSoundType = UserDefaults.standard.string(forKey: "SoundType")!
        }
        songTitle.text = mainSongTitle
        background.image = image
        mainImageView.image = image
        downloadFileFromURL(url: URL(string: mainPreviewURL)!)
        print(mainPreviewURL)
        playpausebtn.setTitle("Pause", for: .normal)
    
    }
    override func viewDidAppear(_ animated: Bool) {
        //deactivateAudioSession()
    }
    
    func deactivateAudioSession() {
        
         do {
            try AVAudioSession.sharedInstance().setActive(false)
            }
        catch let error {
        print(error.localizedDescription)
    }
        
        
    }
    func deactivateAudioSessiondata() {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        }
        catch let error {
            print(error.localizedDescription)
        }
    }
    
    func downloadFileFromURL(url: URL){
        var downloadTask = URLSessionDownloadTask()
        downloadTask = URLSession.shared.downloadTask(with: url, completionHandler: {
            customURL, response, error in
            
            AudioManager.sharedInstance.startMusic(url: customURL!)

           // self.play(url: customURL!)

        })

        downloadTask.resume()

    }
    @IBAction func backButtonAction(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
//60a0Rd6pjrkxjPbaKzXjfq
    func play(url: URL) {
        
        //let urlString = String(describing: url)
        //let path : NSString? = Bundle.main.path(forResource: urlString, ofType: strSoundType)! as NSString
        //let urlPlay = NSURL(fileURLWithPath: path! as String)

        do {
            player = try AVAudioPlayer(contentsOf: url)
            player.prepareToPlay()
           // player.set
            player.play()

        }
        catch{
            print(error)
        }

    }
   
    @IBAction func pauseplay(_ sender: AnyObject) {
        var isCheckPlayPause = false
        AudioManager.sharedInstance.stopMusic()
        
        isCheckPlayPause =  AudioManager.sharedInstance.isCheckPausePlayBtn
        
        if isCheckPlayPause == false{
             playpausebtn.setTitle("Play", for: .normal)
        }
        else{
            playpausebtn.setTitle("Pause", for: .normal)
        }
        
       
//        if player.isPlaying {
//            player.pause()
//            playpausebtn.setTitle("Play", for: .normal)
//        }
//        else{
//            player.play()
//            playpausebtn.setTitle("Pause", for: .normal)
//        }
}

}


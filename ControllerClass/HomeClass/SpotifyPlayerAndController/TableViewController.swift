//
//  TableViewController.swift
//  Broski
//
//  Created by deepkohli on 13/01/20.
//  Copyright © 2020 Webmobril.com. All rights reserved.
//

import UIKit
@_exported import Alamofire
import AVFoundation
import SVProgressHUD
import NVActivityIndicatorView

var player = AVAudioPlayer()

struct post {
    let mainImage : UIImage!
    let name : String!
    let type : String!
    let struri : String!
  let previewURL : String?
}

class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, SPTAudioStreamingPlaybackDelegate, SPTAudioStreamingDelegate,NVActivityIndicatorViewable {
    var auth = SPTAuth.defaultInstance()!
    var session:SPTSession!
    var player: SPTAudioStreamingController?
    var loginUrl: URL?
    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var searchText: UITextField!
    @IBOutlet var myPlayListBtn: UIButton!
    var posts = [post]()
    var searchURL = String()
    typealias JSONStandard = [String : AnyObject]
    private var isSearchResults = Bool.self
    var searchResults = [[String : Any]]()
    //zh0bxrk8o2yingr5jq2b7mypk
    //55pkszSwcbvNgPAvYkDpq3
    //55pkszSwcbvNgPAvYkDpq3
    //0fYVliAYKHuPmECRs1pbRf
    //https://api.spotify.com/v1/users/zh0bxrk8o2yingr5jq2b7mypk/playlists
  

    override func viewDidLoad() {
        super.viewDidLoad()
        myPlayListBtn.layer.cornerRadius = 5.0
        myPlayListBtn.clipsToBounds = true
        self.navigationController?.isNavigationBarHidden = true
        UserDefaults.standard.set(false, forKey: "enableSearch")
        callToken()
        SVProgressHUD.show()
        let defaultURL = "https://api.spotify.com/v1/search?q=Linkin+Park&type=track&limit=15&access_token=\(AuthService.instance.tokenId ?? "")"
        print(defaultURL)
        //self.showToast(message: "Please wait, it will take some time..")
     callAlamofire(url: defaultURL)
     self.tableView.endUpdates()
     SVProgressHUD.dismiss()
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    
    @IBAction func playlistButtonTapped(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "PlayVC") as! PlayVC
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
    @IBAction func searchTapped(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "enableSearch")
     
        let keywords = searchText.text
        let finalKeywords = keywords?.replacingOccurrences(of: " ", with: "+")

        searchURL = "https://api.spotify.com/v1/search?q=\(finalKeywords!)&type=track&&limit=5&access_token=\(AuthService.instance.tokenId ?? "")"
         self.view.endEditing(true)
        print(searchURL)
        SVProgressHUD.show()
        posts.removeAll()
        callAlamofire(url: searchURL)
         self.tableView.reloadData()
        //self.showToast(message: "Please wait, it will take some time..")
        SVProgressHUD.dismiss()
       
     
    }
    
    @IBAction func backButtonAction(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func callAlamofire(url: String){
        //let size = CGSize(width: 30, height: 30)
           // startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
        AF.request(url).responseJSON(completionHandler: {
            response in
           
            self.parseData(JSONData: response.data!)
        })
    }
    func parseData(JSONData : Data) {
        do {
            var readableJSON = try JSONSerialization.jsonObject(with: JSONData, options: .mutableContainers) as! JSONStandard
           
            if let tracks = readableJSON["tracks"] as? JSONStandard{
                // SVProgressHUD.show()
               
                if let items = tracks["items"] as? [JSONStandard] {
                    for i in 0..<items.count{
                        let item = items[i]
                        print(item)
                        let name = item["name"] as! String
                        let type = item["type"] as! String
                        let struri = item["uri"] as! String
                        print(struri)
                        
                       if let previewURL = item["preview_url"] as? String
                       {
                        if let album = item["album"] as? JSONStandard{
                            if let images = album["images"] as? [JSONStandard]{
                                let imageData = images[0]
                                let mainImageURL =  URL(string: imageData["url"] as! String)
                                let mainImageData = NSData(contentsOf: mainImageURL!)
                                let mainImage = UIImage(data: mainImageData as! Data)
                                posts.append(post.init(mainImage: mainImage, name: name, type: type, struri: struri, previewURL: previewURL))
                               DispatchQueue.main.async {
                                self.stopAnimating(nil)
                                    }
                                self.tableView.reloadData()
                            }}}else {
                        DispatchQueue.main.async {
                        self.stopAnimating(nil)
                            }
                       // SVProgressHUD.dismiss()
                        let alert = UIAlertController(title: "Spotify Limitations:", message: "Preview link not available enough, try some other artists.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                       self.present(alert, animated: true)
                        print("No Preview URL")
                        } }}
            }
        }
        catch{
             SVProgressHUD.dismiss()
            print(error)
        }
        
        
        
    }
    func callToken() {
        let parameters = ["client_id" : "b875b386c1cf416db21057cb794083e8",
                          "client_secret" : "df324133fc8e4c869b04e47c92372f5d",
                          "grant_type" : "client_credentials"]
        AF.request("https://accounts.spotify.com/api/token", method: .post, parameters: parameters).responseJSON(completionHandler: {
            response in
            print(response)
            print(response.result)
            
            switch response.result {
            case .success(let value):
                let json = value as? [String: Any]
                
                AuthService.instance.tokenId = json?["access_token"] as? String
                
//                var errorres = false
//                if json!["error"] as? Bool != nil{
//                 errorres = json!["error"] as! Bool
//                }
//                if json!["error"] as? String != nil{
//                  let error:String = json!["error"] as! String
//                  if error == "false"{
//                      errorres = false
//                  }
//                  else{
//                      errorres = true
//                  }
//                }
//
//                if !errorres{
//                }
                
            case .failure(let error):
                print(error)
               
            }
            
            
//            if let result = response.result.value {
//                let jsonData = result as! NSDictionary
//                AuthService.instance.tokenId = jsonData.value(forKey: "access_token") as? String
//                print(AuthService.instance.tokenId!)
//
//
//            }
        })
       
    }
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    let isSearchResults: Bool = UserDefaults.standard.bool(forKey: "enableSearch")
   return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayListCell") as! PlayListCell
        cell.selectionStyle = .none
        cell.mainImageView.image = posts[indexPath.row].mainImage
        cell.musicNameLabel.text = posts[indexPath.row].name
        cell.musictypeLabel.text = posts[indexPath.row].type
        return cell
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //let vc = Router.sharedInst().getTrackListView()
        
//        let vc = Router.sharedInst().getAudioView()
//        vc.image = posts[indexPath.row].mainImage
//        vc.mainSongTitle = posts[indexPath.row].name
//        vc.tryStrUri = posts[indexPath.row].struri
//        vc.mainPreviewURL = posts[indexPath.row].previewURL!
//        self.navigationController?.pushViewController(vc, animated: true)
        
        let vvc = EqualizerTypeListViewController(nibName: "EqualizerTypeListViewController", bundle: nil)
                vvc.previewUrl = posts[indexPath.row].previewURL!
                vvc.titleName = posts[indexPath.row].name
        //    self.present(vvc, animated: true, completion: nil)
                self.navigationController?.pushViewController(vvc, animated: true)
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 137.0;//Choose your custom row height
    }
    func deactivateAudioSession() {
        do {
            try SPTAudioStreamingController.sharedInstance().stop()
            SPTAuth.defaultInstance().session = nil
        } catch let error {
            let alert = UIAlertController(title: "Error deinit", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: { })
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indexPath = self.tableView.indexPathForSelectedRow?.row
        
        let vc = segue.destination as! AudioVC
        
        vc.image = posts[indexPath!].mainImage
        vc.mainSongTitle = posts[indexPath!].name
        vc.tryStrUri = posts[indexPath!].struri
        vc.mainPreviewURL = posts[indexPath!].previewURL!
    }
    
   
}

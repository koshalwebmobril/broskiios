//
//  TrackListVC.swift
//  Broski
//
//  Created by deepkohli on 23/01/20.
//  Copyright © 2020 Webmobril.com. All rights reserved.
//

import UIKit
import WebKit

class TrackListVC: UIViewController,WKNavigationDelegate {
    
     @IBOutlet weak var webView: WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
         webView.navigationDelegate = self
        
 MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
       let myURL = URL(string: "https://open.spotify.com/artist/6XyY86QOPPrYVGvF9ch6wz")
            let myRequest = URLRequest(url: myURL!)
            webView.load(myRequest)
    }
    
    //MARK:- WKNavigationDelegate
    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
    MBProgressHUD.hideHUD()
    MBProgressHUD.hide(for: (self.view)!, animated: true)
    }
    private func webView(webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    MBProgressHUD.hideHUD()
    MBProgressHUD.hide(for: (self.view)!, animated: true)
    }
    
    
    
    @IBAction func action_back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}

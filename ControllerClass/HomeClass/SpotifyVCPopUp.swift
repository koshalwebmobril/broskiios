//
//  SpotifyVCPopUp.swift
//  Broski
//
//  Created by deepkohli on 26/12/19.
//  Copyright © 2019 Webmobril.com. All rights reserved.
//

import UIKit

class SpotifyVCPopUp: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var spotifyIcon: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
    }
    @IBAction func action_touchAnyWhere(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func action_justOnce(_ sender: Any) {
        //self.dismiss(animated: true, completion: nil)
        let vc = Router.sharedInst().getSpotifyLoginView()
        self.navigationController?.pushViewController(vc, animated: true)
    }
       @IBAction func action_always(_ sender: Any) {
       // self.dismiss(animated: true, completion: nil)
         let vc = Router.sharedInst().getSpotifyLoginView()
         self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//
//  ContactUsVC.swift
//  Broski
//
//  Created by deepkohli on 03/01/20.
//  Copyright © 2020 Webmobril.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class ContactUsVC: UIViewController,UITextViewDelegate,NVActivityIndicatorViewable {
    
    @IBOutlet weak var titleTextFild: UITextField!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var viewForTextFiled: UIView!
    @IBOutlet weak var viewForTextView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        messageTextView.textColor = UIColor.lightGray
        messageTextView.text = "Description"
        setUpIntial()
    }
    
    func setUpIntial(){
        viewForTextView.layer.cornerRadius = 5
        viewForTextView.layer.borderWidth = 1.0
        viewForTextView.layer.borderColor = UIColor.black.cgColor
        viewForTextView.clipsToBounds = true
        viewForTextFiled.layer.cornerRadius = 5
        viewForTextFiled.layer.borderWidth = 1.0
        viewForTextFiled.layer.borderColor = UIColor.black.cgColor
        viewForTextFiled.clipsToBounds = true
        cancelButton.layer.cornerRadius = cancelButton.frame.size.height / 2
        cancelButton.clipsToBounds = true
        submitButton.layer.cornerRadius = submitButton.frame.size.height / 2
        submitButton.clipsToBounds = true
        messageTextView.delegate = self
       
    }
    func helpAndSupportApi(){
            self.view.endEditing(true)
              if Reachability.isConnectedToNetwork() {
                let user_id = UserDefaults.standard.value(forKey: "IdVal") as? String ?? ""
              let apiURL: String = "\(BASEURL)\(CONTACTADMIN)"
              print("\(apiURL)")
                let newTodo: [String: Any] = ["userid": user_id,"subject": titleTextFild.text!,"message": messageTextView.text!]
                  let size = CGSize(width: 30, height: 30)
              startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
                
              AF.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in
                  switch response.result {
                   case .success(let value):
                      print(response)
                      
                      let json = value as? [String: Any]
                      
                      var errorres = false
                      if json!["error"] as? Bool != nil{
                       errorres = json!["error"] as! Bool
                      }
                      if json!["error"] as? String != nil{
                        let error:String = json!["error"] as! String
                        if error == "false"{
                            errorres = false
                        }
                        else{
                            errorres = true
                        }
                      }
                      
                      if !errorres{
                       // let resultDict = json!["result"] as? [String: Any]
                        
                       let dialogMessage = UIAlertController(title: AppName, message: (json!["message"] as! String), preferredStyle: .alert)
                              let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
                                self.navigationController?.popViewController(animated: true)
                              })
                              dialogMessage.addAction(ok)
                              self.present(dialogMessage, animated: true, completion: nil)
                      }
                      else{
                         FXAlertController.alert(AppName, message: json!["message"] as! String)
                      }
                      DispatchQueue.main.async {
                        self.stopAnimating(nil)
                      }
                      break
                      
                  case .failure(let error):
                      print(error)
                      DispatchQueue.main.async {
                     self.stopAnimating(nil)
                      }
                  }
               }
              }
    }
    
    
    func dataValidation() -> Bool {
    let whiteSpaces = CharacterSet.whitespacesAndNewlines
        if titleTextFild.text!.isEmpty || titleTextFild.text!.trimmingCharacters(in: whiteSpaces).isEmpty {
            FXAlertController.alert(AppName, message: MessageTitle)
            titleTextFild.becomeFirstResponder()
            return false
        }
        if messageTextView.text!.isEmpty || messageTextView.text! == "Description" {
            FXAlertController.alert(AppName, message: "Please enter description")
            messageTextView.becomeFirstResponder()
            return false
        }
        
        return true
    }
    
    
    
    // UITextView Delegate Method
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Description"
            textView.textColor = UIColor.lightGray
        }
    }
    @IBAction func settingButtonAction(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func cancelButtonAction(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    }
    @IBAction func submitButtonAction(_ sender: Any) {
       if self.dataValidation() {
        helpAndSupportApi()
        }
    }
    
}

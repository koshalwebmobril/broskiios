//
//  WebViewVC.swift
//  Broski
//
//  Created by deepkohli on 01/01/20.
//  Copyright © 2020 Webmobril.com. All rights reserved.
//

import UIKit
import WebKit
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON


class WebViewVC: UIViewController,WKNavigationDelegate,NVActivityIndicatorViewable {
    
    var labeltitle = String()
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var lbl_title: UILabel!
    var urlString = String()

    override func viewDidLoad() {
        super.viewDidLoad()
         webView.navigationDelegate = self

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lbl_title.text = labeltitle
        
         getWebViewData()
    
//        if lbl_title.text == "Privacy Policy"{
//            urlString = "https://webmobril.org/dev/broski_app/api/api/privacypolicy?pageid=1"
//        }
//        else if lbl_title.text == "Terms & Conditions"{
//            urlString = "https://webmobril.org/dev/broski_app/api/api/tnc?pageid=2"
//        }
       
//         let size = CGSize(width: 30, height: 30)
//         startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
//         let myURL = URL(string: urlString)
//         let myRequest = URLRequest(url: myURL!)
//         webView.load(myRequest)
       
    }
    func getWebViewData(){
    if Reachability.isConnectedToNetwork() {
        var urlString = String()
        let size = CGSize(width: 30, height: 30)
        self.startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
    if lbl_title.text == "Privacy Policy"{
        urlString = "https://webmobril.org/dev/broski_app/api/api/privacypolicy?pageid=1"
    }
    else if lbl_title.text == "Terms & Conditions"{
        urlString = "https://webmobril.org/dev/broski_app/api/api/tnc?pageid=2"
    }
    print(urlString)
    AF.request(urlString, method: .get, encoding: JSONEncoding.default, headers: nil).responseJSON {
                        response in
                        switch response.result {
                         case .success(let value):
                            print(response)
                            
                            let json = value as? [String: Any]
                            
                            let errorres = json!["error"] as! Bool
                            
                            if errorres {
                                FXAlertController.alert(AppName, message: json!["message"] as! String)
                            }
                            else{
                                let resultDict = json!["result"] as? [String: Any]
                                 if resultDict!["description"] as? String != nil{
                                    let strDescription:String = resultDict!["description"] as! String
                                    self.webView.loadHTMLString(strDescription, baseURL: nil)
                                }
                            }
                            DispatchQueue.main.async {
                              self.stopAnimating(nil)
                            }
                            break
                        case .failure(let error):
                            print(error)
                            DispatchQueue.main.async {
                           self.stopAnimating(nil)
                            }
                        }
                    }
                }
    }
    
    //MARK:- WKNavigationDelegate
    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
   DispatchQueue.main.async {
    self.stopAnimating(nil)
    }
    }
    private func webView(webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
   let size = CGSize(width: 30, height: 30)
    startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
        }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    DispatchQueue.main.async {
       self.stopAnimating(nil)
       }
    }
    @IBAction func btn_SettingsAction(_ sender: Any) {
       let vc = Router.sharedInst().getAccountSettingsView()
        self.navigationController?.pushViewController(vc, animated: true)
    }
     @IBAction func btn_BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

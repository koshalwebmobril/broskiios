//
//  AccountSettingsVC.swift
//  Broski
//
//  Created by deepkohli on 26/12/19.
//  Copyright © 2019 Webmobril.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import SDWebImage

class AccountSettingsVC: UIViewController,NVActivityIndicatorViewable,updateProfileRecordDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    
    let listArray = ["Home","Back to iTuneMusic","Terms and Conditions","Privacy Policy","Change Password","Contact Us","Sign Out"]
    var modelArray: Array<AccountSettingsModel> = []

    override func viewDidLoad() {
        super.viewDidLoad()
         setupInitialVal()
         getUserProfileData()
    }
    override func viewWillAppear(_ animated: Bool) {
       
    }
    func setupInitialVal(){
        userImageView.layer.cornerRadius = userImageView.frame.size.width / 2
        userImageView.clipsToBounds = true
        userImageView.layer.borderWidth = 4.0
        userImageView.layer.borderColor = UIColor.white.cgColor
    }
    func updateProfileRecord(dict:[String:Any]){
        self.modelArray = AccountSettingsModel.getUserDetailsData(response: dict)
        setUPUserProfileData()
    }
    func setUPUserProfileData(){
        var fName = String()
        var lName = String()
        
        fName = modelArray[0].firstName
        lName = modelArray[0].lastName
        
        if !fName.isEmpty && !lName.isEmpty{
        userNameLabel.text = String(format: "%@%@%@",fName," ",lName)
        }
        else{
            userNameLabel.text = "Not Available"
        }
        if modelArray[0].email != nil {
        userEmailLabel.text = modelArray[0].email
        }
        if modelArray[0].userImage.isEmpty{
           self.userImageView.image = UIImage(named: "user")
        }
        else{
            userImageView.sd_imageIndicator = SDWebImageActivityIndicator.white
            let image = modelArray[0].userImage
            userImageView.sd_setImage(with: URL(string:image!)) { (image, error, cache, urls) in
                            if (error != nil) {
                                self.userImageView.image = UIImage(named: "user")
                            } else {
                                self.userImageView.image = image
                                self.userImageView.contentMode = .scaleToFill

                            }
                }

        }
       
    }
    func getUserProfileData(){
    if Reachability.isConnectedToNetwork() {
        let user_id = UserDefaults.standard.value(forKey: "IdVal") as? String ?? ""
        let size = CGSize(width: 30, height: 30)
        self.startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
    let urlString = String(format: "https://webmobril.org/dev/broski_app/api/api/getprofile?userid=%@",user_id)
    print(urlString)
    AF.request(urlString, method: .get, encoding: JSONEncoding.default, headers: nil).responseJSON {
                        response in
                        switch response.result {
                         case .success(let value):
                            print(response)
                            
                            let json = value as? [String: Any]
                            
                            let errorres = json!["error"] as! Bool
                            
                            if !errorres {
                                if json!["result"] as? [String: Any] != nil{
                                    self.modelArray = AccountSettingsModel.getUserDetailsData(response: (json!["result"] as? [String: Any])!)
                                self.setUPUserProfileData()
                                }
                            }
                            
                            DispatchQueue.main.async {
                              self.stopAnimating(nil)
                            }
                            break
                            
                        case .failure(let error):
                            print(error)
                            DispatchQueue.main.async {
                           self.stopAnimating(nil)
                            }
                        }
                    }
                }
    }
    func apiLogout(){
        self.view.endEditing(true)
          if Reachability.isConnectedToNetwork() {
            let user_id = UserDefaults.standard.value(forKey: "IdVal") as? String ?? ""
          let apiURL: String = "\(BASEURL)\(APILOGOUT)"
          print("\(apiURL)")
          let newTodo: [String: Any] = ["userid": user_id]
              let size = CGSize(width: 30, height: 30)
          startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
          AF.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in
              switch response.result {
               case .success(let value):
                  print(response)
                  
                  let json = value as? [String: Any]
                  
                  var errorres = false
                  if json!["error"] as? Bool != nil{
                   errorres = json!["error"] as! Bool
                  }
                  if json!["error"] as? String != nil{
                    let error:String = json!["error"] as! String
                    if error == "false"{
                        errorres = false
                    }
                    else{
                        errorres = true
                    }
                  }
                  
                  if !errorres{
                   
                   let dialogMessage = UIAlertController(title: AppName, message: "Are you sure you want to Logout?", preferredStyle: .alert)
                          let ok = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
                            UserDefaults.standard.set(false, forKey: "IsLogin")
                            UserDefaults.standard.set(false, forKey: "ISPlaySong")
                            
                            appDelegate.loginView()
                          })
                          let cancel = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
                              print("Cancel button click...")
                    }
                          dialogMessage.addAction(ok)
                          dialogMessage.addAction(cancel)
                          self.present(dialogMessage, animated: true, completion: nil)
                    
                  }
                  else{
                     FXAlertController.alert(AppName, message: json!["message"] as! String)
                  }
                  DispatchQueue.main.async {
                    self.stopAnimating(nil)
                  }
                  break
                  
              case .failure(let error):
                  print(error)
                  DispatchQueue.main.async {
                 self.stopAnimating(nil)
                  }
              }
           }
          }
    }
    @IBAction func btn_editProfileAction(_ sender: Any) {
        let vc = Router.sharedInst().getEditProfileView()
            vc.delegate = self
            vc.modelArray = modelArray
        self.navigationController?.pushViewController(vc, animated: true)
       }
    @IBAction func action_headPhone(_ sender: Any) {
           
       }
    @IBAction func action_power(_ sender: Any) {
           
       }
    @IBAction func action_setting(_ sender: Any) {
        
      }
}

extension AccountSettingsVC : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return listArray.count
        }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingTableViewCell", for: indexPath) as! SettingTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.nameLabel?.text = listArray[indexPath.row]
            return cell
        }
       
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            appDelegate.homeView()
        }
        if indexPath.row == 1{
            self.navigationController?.popViewController(animated: true)
        }
        if indexPath.row == 2{
            let vc = Router.sharedInst().getWebViewView()
            vc.labeltitle = "Terms & Conditions"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 3{
            let vc = Router.sharedInst().getWebViewView()
            vc.labeltitle = "Privacy Policy"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 4{
           let vc = Router.sharedInst().getChangePasswordView()
           self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 5{
            let vc = Router.sharedInst().getContactUsView()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 6{
            //self.apiLogout()
            let dialogMessage = UIAlertController(title: AppName, message: "Are you sure you want to Logout?", preferredStyle: .alert)
                  let ok = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
                    UserDefaults.standard.set(false, forKey: "IsLogin")
                     UserDefaults.standard.removeObject(forKey: "SpotifyLogin")
                    appDelegate.loginView()
                  })
                  let cancel = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
                      print("Cancel button click...")
            }
                  dialogMessage.addAction(ok)
                  dialogMessage.addAction(cancel)
                  self.present(dialogMessage, animated: true, completion: nil)
        }
       
        }
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
      }
      @objc func rechargedButtonClicked() {
      }
}

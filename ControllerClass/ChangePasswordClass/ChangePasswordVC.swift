//
//  ChangePasswordVC.swift
//  Broski
//
//  Created by deepkohli on 03/01/20.
//  Copyright © 2020 Webmobril.com. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class ChangePasswordVC: UIViewController,NVActivityIndicatorViewable {
    
    @IBOutlet weak var btn_ChangePassword: UIButton!
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var newPasswordView: UIView!
    @IBOutlet weak var oldPasswordView: UIView!
    @IBOutlet weak var txt_newPassword: UITextField!
    @IBOutlet weak var txt_confirmPassword: UITextField!
    @IBOutlet weak var txt_oldPassword: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInitialValue()

    }
    func setUpInitialValue(){
        btn_ChangePassword.layer.cornerRadius = btn_ChangePassword.frame.size.height / 2
        btn_ChangePassword.clipsToBounds = true
        confirmPasswordView.dropBorder()
        newPasswordView.dropBorder()
        oldPasswordView.dropBorder()
       
    }
    func dataValidation() -> Bool {
    let whiteSpaces = CharacterSet.whitespacesAndNewlines
        if txt_oldPassword.text!.isEmpty || txt_oldPassword.text!.trimmingCharacters(in: whiteSpaces).isEmpty {
            FXAlertController.alert(AppName, message: OLDPassword)
            txt_oldPassword.becomeFirstResponder()
            return false
        }
        if txt_newPassword.text!.isEmpty || txt_newPassword.text!.trimmingCharacters(in: whiteSpaces).isEmpty {
            FXAlertController.alert(AppName, message: NewPassword)
            txt_newPassword.becomeFirstResponder()
            return false
        }
        if txt_confirmPassword.text!.isEmpty || txt_confirmPassword.text!.trimmingCharacters(in: whiteSpaces).isEmpty {
            FXAlertController.alert(AppName, message: EmptyConfirmPassword)
            txt_newPassword.becomeFirstResponder()
            return false
        }
       if txt_newPassword.text! != txt_confirmPassword.text!
       {
           txt_newPassword.becomeFirstResponder()
           FXAlertController.alert(AppName, message: PasswordMatch)
           return false
       }
        return true
    }
    func apiChangePassword(){
        self.view.endEditing(true)
          if self.dataValidation() {
          if Reachability.isConnectedToNetwork() {
          let apiURL: String = "\(BASEURL)\(APICHANGEPassword)"
          print("\(apiURL)")
            var devicetoken = String()
            if UserDefaults.standard.value(forKey: "DEVICETOKEN") as? String != nil{
            devicetoken = UserDefaults.standard.value(forKey: "DEVICETOKEN") as! String
            }
            else{
            devicetoken = "fqm7qMYw9X4:APA91bGxsAu2mWut0v1icc4Dj9whCOUF4MP5HphwjvTpit3ym7o1_0TnPRKMyLD3XHmPVnRbMEf5cAOkyzX1A8Ox9QLAj1PCfXrHtINFDbeQFqTrQOH2EqZ7_JxovOwFZDuLbUfCsmBy"
            }
            let newTodo: [String: Any] = ["newpass": txt_newPassword.text ?? "123","oldPassword": txt_oldPassword.text!,"cpass": txt_confirmPassword.text!,"device_type":DeviceType,"device_token": devicetoken]
              let size = CGSize(width: 30, height: 30)
          startAnimating(size, message: "Please wait...", type: .ballPulseSync , fadeInAnimation: nil)
          AF.request(apiURL, method: .post, parameters: newTodo, encoding: URLEncoding.default).responseJSON { response in
              switch response.result {
               case .success(let value):
                  print(response)
                  
                  let json = value as? [String: Any]
                 
                  var errorres = false
                  if json!["error"] as? Bool != nil{
                   errorres = json!["error"] as! Bool
                  }
                  if json!["error"] as? String != nil{
                    let error:String = json!["error"] as! String
                    if error == "false"{
                        errorres = false
                    }
                    else{
                        errorres = true
                    }
                  }
                   
                   if !errorres {
                      let alertController = UIAlertController(title: AppName, message: (json!["message"] as! String), preferredStyle: .alert)
                      let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                          UIAlertAction in
                        self.navigationController?.popViewController(animated: true)
                      }
                      alertController.addAction(okAction)
                      self.present(alertController, animated: true, completion: nil)
                   }
                   else{
                     FXAlertController.alert(AppName, message: json!["message"] as! String)
                   }
                  
                  DispatchQueue.main.async {
                    self.stopAnimating(nil)
                  }
                  break
                  
              case .failure(let error):
                  print(error)
                  DispatchQueue.main.async {
                 self.stopAnimating(nil)
                  }
              }
           }
          }
        }
    }
    @IBAction func action_SubmitAction(_ sender: Any) {
        apiChangePassword()
    }
    @IBAction func btn_BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func action_headPhone(_ sender: Any) {
           
       }
    @IBAction func action_power(_ sender: Any) {
           
       }
    @IBAction func action_setting(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
}
